#include <sys/times.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

void display_process_times(const char *msg);

int main(int argc, char *argv[])
{
    long count = 0;
    char *endptr;
    int i;

    if (argc < 2 || strcmp(argv[0], "--help") == 0) {
        printf("Usage: %s loop-count\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    errno = 0;
    strtol(argv[1], &endptr, 10);
    if (errno != 0) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }

    if (*argv[1] == '\0' || *endptr != '\0') {
        printf("%s: Invalid string given\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    printf("CLOCKS_PER_SEC=%ld\tsysconf(_SC_CLK_TCK)=%ld\n\n",
        (long) CLOCKS_PER_SEC, sysconf(_SC_CLK_TCK));

    display_process_times("At program start:");

    for (i = 0; i < count; ++i)
        (void) getppid();

    display_process_times("After getppid() loop:");

    exit(EXIT_SUCCESS);
}

void display_process_times(const char *msg)
{
    struct tms t;
    clock_t tot_cpu;
    double proctime_sec;
    static long clock_ticks;

    if (clock_ticks == 0) {
        clock_ticks = sysconf(_SC_CLK_TCK);
        if (clock_ticks == -1) {
            perror("sysconf");
            exit(EXIT_FAILURE);
        }
    }

    tot_cpu = clock();
    if (tot_cpu == -1) {
        perror("clock");
        exit(EXIT_FAILURE);
    }

    proctime_sec = (double) tot_cpu / CLOCKS_PER_SEC;

    if (times(&t) == -1) {
        perror("times");
        exit(EXIT_FAILURE);
    }

    printf("%s\n", msg);
    printf("\tclock() returns: %ld clocks-per-sec (%.3f secs)\n",
        tot_cpu, proctime_sec);
    printf("\ttimes() yields: user CPU=%.3f system CPU=%.3f\n",
        (double) (t.tms_utime / clock_ticks),
        (double) (t.tms_stime / clock_ticks));
}
