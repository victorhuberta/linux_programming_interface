#define _XOPEN_SOURCE

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>

#define MAX_OUTPUT_SIZE 256
#define DEFAULT_W_FORMAT "%H:%M:%S %A, %d %B %Y %Z"

int main(int argc, char *argv[])
{
    time_t t;
    struct tm tm;
    char *input, *r_fmt, *w_fmt;
    char output[MAX_OUTPUT_SIZE] = {0};

    if (argc < 3 || strcmp(argv[1], "--help") == 0) {
        printf("Usage: %s date-time read-format [write-format]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (setlocale(LC_ALL, "") == NULL) {
        perror("setlocale");
        exit(EXIT_FAILURE);
    }

    input = argv[1];
    r_fmt = argv[2];
    w_fmt = (argc > 3) ? argv[3] : DEFAULT_W_FORMAT;

    memset(&tm, 0, sizeof(struct tm));

    if (strptime(input, r_fmt, &tm) == NULL) {
        perror("strptime");
        exit(EXIT_FAILURE);
    }

    tm.tm_isdst = -1; /* make mktime to decide if DST is in effect */

    t = mktime(&tm);
    if (t == -1) {
        perror("mktime");
        exit(EXIT_FAILURE);
    }

    printf("calendar time (seconds since Epoch): %ld\n", (long) t);

    if (strftime(output, MAX_OUTPUT_SIZE, w_fmt, &tm) == 0) {
        puts("Empty date time given or an error has occurred.");
        exit(EXIT_FAILURE);
    }

    printf("strftime yields: %s\n", output);

    exit(EXIT_SUCCESS);
}
