#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#define SECS_IN_TROPICAL_YEARS (365.242189 * 24 * 60 * 60)

int main(void)
{
    time_t t, gmt, lct;
    double t_yrs;
    struct timeval tv;
    struct tm *gmp, gm, *lcp, lc;
    char *t_str;

    t = time(NULL);
    t_yrs = t / SECS_IN_TROPICAL_YEARS;

    gettimeofday(&tv, NULL); /* no error */

    gmp = gmtime(&t);
    if (gmp == NULL) {
        perror("gmtime");
        exit(EXIT_FAILURE);
    }
    gm = *gmp;

    lcp = localtime(&t);
    if (lcp == NULL) {
        perror("localtime");
        exit(EXIT_FAILURE);
    }
    lc = *lcp;

    printf("Seconds since the Epoch (1 Jan 1970): %ld (about %.3f years).\n",
        t, t_yrs);
    printf("\tgettimeofday() returned %ld secs, %ld microsecs.\n",
        (long) tv.tv_sec, (long) tv.tv_usec);

    printf("Broken down by gmtime():\n\tyear=%d mon=%d mday=%d hour=%d min=%d "
        "sec=%d wday=%d yday=%d isdst=%d\n", gm.tm_year, gm.tm_mon, gm.tm_mday,
        gm.tm_hour, gm.tm_min, gm.tm_sec, gm.tm_wday, gm.tm_yday, gm.tm_isdst);

    printf("Broken down by localtime():\n\tyear=%d mon=%d mday=%d hour=%d "
        "min=%d sec=%d wday=%d yday=%d isdst=%d\n", lc.tm_year, lc.tm_mon,
        lc.tm_mday, lc.tm_hour, lc.tm_min, lc.tm_sec, lc.tm_wday, lc.tm_yday,
        lc.tm_isdst);

    t_str = asctime(&gm);
    if (t_str == NULL) {
        perror("asctime");
        exit(EXIT_FAILURE);
    }

    printf("asctime() formats the gmtime() value as: %s", t_str);

    t_str = ctime(&t);
    if (t_str == NULL) {
        perror("ctime");
        exit(EXIT_FAILURE);
    }

    printf("ctime() formats the time() value as: %s", t_str);

    gmt = mktime(&gm);
    if (gmt == -1) {
        perror("mktime");
        exit(EXIT_FAILURE);
    }

    lct = mktime(&lc);
    if (lct == -1) {
        perror("mktime");
        exit(EXIT_FAILURE);
    }

    printf("mktime() of gmtime() value: %ld secs\n", (long) gmt);
    printf("mktime() of localtime() value: %ld secs\n", (long) lct);

    exit(EXIT_SUCCESS);
}
