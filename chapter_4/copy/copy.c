#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#ifndef MAX_BUFFER_LEN
#define MAX_BUFFER_LEN 1024
#endif

int if_error_handle(int res, const char *fname);

int main(int argc, char *argv[]) {
    if (argc != 3 || strcmp(argv[1], "--help") == 0) {
        puts("Usage: ./copy old_name new_name");
        exit(EXIT_FAILURE);
    }

    int old_fd = if_error_handle(open(argv[1], O_RDONLY), "open");

    int open_flags = O_CREAT | O_WRONLY | O_TRUNC;
    mode_t file_perms = 0666; /* -rw-rw-rw- */
    int new_fd = if_error_handle(open(argv[2], open_flags, file_perms), "open");

    ssize_t num_read = 0;
    char buffer[MAX_BUFFER_LEN] = {0};

    while ((num_read = read(old_fd, buffer, MAX_BUFFER_LEN)) > 0) {
        ssize_t num_written = write(new_fd, buffer, num_read);
        if (num_written != num_read) {
            fprintf(stderr, "Read: %ld bytes; Written: %ld bytes.\n",
                (long) num_read, (long) num_written);
            exit(EXIT_FAILURE);
        }
    }

    if_error_handle((int) num_read, "read");
    if_error_handle(close(new_fd), "close");
    if_error_handle(close(old_fd), "close");

    exit(EXIT_SUCCESS);
}

int if_error_handle(int res, const char *fname) {
    if (res == -1) {
        perror(fname);
        exit(EXIT_FAILURE);
    }
    return res;
}
