#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <getopt.h>

#define MAX_TOTAL_LEN 1024
#define MAX_BUF_LEN 120

void write_to_file(const char *filename, int open_flags,
        const char *buf, int buf_len);

int main(int argc, char *argv[]) {
    if (argv[1] != NULL && strcmp(argv[1], "--help") == 0) {
        puts("Usage: ./tee file {-a}");
        exit(EXIT_FAILURE);
    }

    int open_flags = O_WRONLY | O_CREAT;

    char opt;
    if ((opt = getopt(argc, argv, "a")) == -1) {
        open_flags |= O_TRUNC;
    } else {
        switch (opt) {
            case 'a':
                open_flags |= O_APPEND;
                break;
            case '?':
                open_flags |= O_TRUNC;
                break;
            default:
                break;
        }
    }

    char buf[MAX_BUF_LEN] = {0};

    size_t num_read = 0;
    while ((num_read = read(STDIN_FILENO, buf, MAX_BUF_LEN))) {
        if (num_read > 0) {
            fwrite(buf, sizeof(char), num_read, stdout);
            if (argv[1] != NULL) {
                write_to_file(argv[1], open_flags, buf, (int) num_read);
                open_flags = O_WRONLY | O_APPEND;
            }
        } else break;
    }

    exit(EXIT_FAILURE);
}

void write_to_file(const char *filename, int open_flags,
        const char *buf, int buf_len) {

    int fd = open(filename, open_flags, 0666); /* -rw-rw-rw- */
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    if (write(fd, buf, buf_len) == -1) {
        perror("write");
        exit(EXIT_FAILURE);
    }

    if (close(fd) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }
}
