#include <sys/stat.h>
#include <sys/unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

void seek_offset(int fd, const char *seek_ofst);
void read_text(int fd, const char *bytes_num, int as_hex);
size_t get_long(const char *str);
void write_text(int fd, const char *text);

int main(int argc, char *argv[]) {
    if (argc < 3 || strcmp(argv[1], "--help") == 0) {
        puts("Usage: ./seek_io file "
            "{s<offset>|r<length>|R<length>|w<string>}...");
        exit(EXIT_FAILURE);
    }

    int fd = open(argv[1], O_RDWR | O_CREAT, 0666);
    if (fd == -1) {
        perror("fd");
        exit(EXIT_FAILURE);
    }

    for (int i = 2; i < argc; ++i) {
        switch (argv[i][0]) {
            case 's':
                seek_offset(fd, &argv[i][1]);
                break;
            case 'r':
            case 'R':
                read_text(fd, &argv[i][1], (argv[i][0] == 'r') ? 0 : 1);
                break;
            case 'w':
                write_text(fd, &argv[i][1]);
                break;
            default:
                puts("Arguments must start with {srRw}!");
        }
    }

    if (close(fd) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}

void seek_offset(int fd, const char *seek_ofst) {
    if (seek_ofst[0] == '\0') {
        puts("empty string for offset!");
        exit(EXIT_FAILURE);
    }

    size_t len = get_long(seek_ofst);

    if (lseek(fd, len, SEEK_SET) == -1) {
        perror("lseek");
        exit(EXIT_FAILURE);
    }

    printf("s%s: seek succeed.\n", seek_ofst);
}

#ifndef MAX_BUF_LEN
#define MAX_BUF_LEN 1024
#endif

void read_text(int fd, const char *bytes_num, int as_hex) {
    ssize_t num_read = 0;

    size_t len = get_long(bytes_num);

    if (len > MAX_BUF_LEN) {
        printf("%s: the number is too large.\n", bytes_num);
        exit(EXIT_FAILURE);
    }

    char buf[MAX_BUF_LEN] = {0};
    num_read = read(fd, buf, len);
    if (num_read == -1) {
        perror("read");
        exit(EXIT_FAILURE);
    } else if (num_read == 0) {
        printf("%s: end-of-file.\n", bytes_num);
        return;
    }

    printf((as_hex) ? "R%s: " : "r%s: ", bytes_num);

    for (int i = 0; i < num_read; ++i) {
        if (as_hex) {
            printf("%02x ", buf[i]);
        } else {
            printf("%c", (isprint(buf[i]) != 0) ? buf[i] : '?');
        }
    }

    puts("");
}

size_t get_long(const char *str) {
    char *end;

    errno = 0;
    size_t len = strtol(str, &end, 10);

    if (errno != 0) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }

    if (*end != '\0') {
        puts("non numeric characters.");
        exit(EXIT_FAILURE);
    } else {
        return len;
    }
}

void write_text(int fd, const char *text) {
    if (text == NULL || text[0] == '\0') {
        puts("bad text.");
        exit(EXIT_FAILURE);
    }

    ssize_t num_written = write(fd, text, strlen(text));
    if (num_written == -1) {
        perror("write");
        exit(EXIT_FAILURE);
    }

    printf("w%s: wrote %ld bytes.\n", text, (long) num_written);
}
