#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int dup(int fd);
int dup2(int oldfd, int newfd);

int main() {
    int fd = open("file", O_RDONLY);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }
    printf("fd = %d\n", fd);

    int dupfd = dup(fd);
    if (dupfd == -1) {
        perror("dup");
        exit(EXIT_FAILURE);
    }
    printf("dup(fd) = %d\n", dupfd);

    int newfd = dup2(30, 10);
    if (newfd == -1) {
        perror("dup2");
    }

    newfd = dup2(dupfd, dupfd);
    if (newfd == -1) {
        perror("dup2");
    }
    printf("dup2(dupfd, dupfd) = %d\n", newfd);

    newfd = dup2(dupfd, 10);
    if (newfd == -1) {
        perror("dup2");
        exit(EXIT_FAILURE);
    }
    printf("dup2(dupfd, 10) = %d\n", newfd);

    close(fd);
    close(dupfd);
    close(newfd);
    exit(EXIT_SUCCESS);
}

int dup(int fd) {
    return fcntl(fd, F_DUPFD, 3);
}

int dup2(int oldfd, int newfd) {
    if (fcntl(oldfd, F_GETFL) == -1) {
        errno = EBADF;
        return -1;
    }

    if (oldfd == newfd) {
        return newfd;
    }

    if (close(newfd) == -1)
        perror("close");

    return fcntl(oldfd, F_DUPFD, newfd);
}
