#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

struct iovec {
    void *iov_base;
    size_t iov_len;
};

ssize_t readv(int fd, const struct iovec * iov, int iovcnt);
ssize_t writev(int fd, const struct iovec * iov, int iovcnt);

struct hello {
    char world[20];
    int sample;
};

int main() {
    struct iovec iov[3];
    struct hello test;

    ssize_t total_required = 0;

    iov[0].iov_base = &test;
    iov[0].iov_len = sizeof(struct hello);
    total_required += iov[0].iov_len;

    int x = 0;
    iov[1].iov_base = &x;
    iov[1].iov_len = sizeof x;
    total_required += iov[1].iov_len;

#define MAX_BUF_SIZE 20
    char buffer[MAX_BUF_SIZE] = {0};
    iov[2].iov_base = buffer;
    iov[2].iov_len = MAX_BUF_SIZE;
    total_required += iov[2].iov_len;

    int fd = open("file1", O_RDONLY);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    ssize_t total_readv = readv(fd, iov, 3);
    if (total_readv == -1) {
        perror("readv");
        exit(EXIT_FAILURE);
    }
    printf("total_readv = %ld VS total_required = %ld\n",
        total_readv, total_required);

    test.world[19] = '\0';
    printf("struct hello test: world = %s; sample = %x\n",
        test.world, test.sample);
    printf("x = %x\n", x);
    printf("buffer = %s\n", buffer);

    close(fd);

    fd = open("file2", O_WRONLY | O_CREAT, 0600);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    ssize_t total_writev = writev(fd, iov, 3);
    if (total_writev == -1) {
        perror("writev");
        exit(EXIT_FAILURE);
    }
    printf("total_writev = %ld VS total_required = %ld\n",
        total_writev, total_required);

    close(fd);

    exit(EXIT_SUCCESS);
}

ssize_t readv(int fd, const struct iovec * iov, int iovcnt) {
    int total_read = 0;

    for (int i = 0; i < iovcnt; ++i) {
        ssize_t num_read = read(fd, iov[i].iov_base, iov[i].iov_len);
        if (num_read == -1) {
            return -1;
        }

        total_read += num_read;

        if (num_read < iov[i].iov_len) {
            return total_read;
        }
    }

    return total_read;
}

ssize_t writev(int fd, const struct iovec * iov, int iovcnt) {
    int total_written = 0;

    for (int i = 0; i < iovcnt; ++i) {
        ssize_t num_written = write(fd, iov[i].iov_base, iov[i].iov_len);
        if (num_written == -1) {
            return -1;
        }

        total_written += num_written;

        if (num_written < iov[i].iov_len) {
            return total_written;
        }
    }

    return total_written;
}
