#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void compare_fd(int fd1, int fd2);

int main() {
    int fd1 = open("file", O_RDONLY);
    if (fd1 == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }
    printf("fd1 = %d\n", fd1);

    int dupfd1 = dup(fd1);
    if (dupfd1 == -1) {
        perror("dup");
        exit(EXIT_FAILURE);
    }
    printf("dupfd1 = %d\n", dupfd1);

    compare_fd(fd1, dupfd1);

    int fd2 = open("file", O_WRONLY);
    if (fd2 == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }
    printf("fd2 = %d\n", fd2);

    int dupfd2 = dup(fd2);
    if (dupfd2 == -1) {
        perror("dup");
        exit(EXIT_FAILURE);
    }
    printf("dupfd2 = %d\n", dupfd2);

    if (lseek(dupfd2, 10, SEEK_CUR) == -1) {
        puts("lseek");
        exit(EXIT_FAILURE);
    }

    compare_fd(fd2, dupfd2);
    compare_fd(fd1, fd2);

    close(fd1);
    close(dupfd1);
    close(fd2);
    close(dupfd2);
    exit(EXIT_SUCCESS);
}

void compare_fd(int fd1, int fd2) {
    printf("Comparing %d and %d...\n", fd1, fd2);

    off_t ofst1 = lseek(fd1, 0, SEEK_CUR);
    if (ofst1 == -1) {
        perror("lseek");
        exit(EXIT_FAILURE);
    }

    off_t ofst2 = lseek(fd2, 0, SEEK_CUR);
    if (ofst2 == -1) {
        perror("lseek");
        exit(EXIT_FAILURE);
    }

    printf("offset1 = %lld VS offset2 = %lld\n",
        (long long) ofst1, (long long) ofst2);

    if (ofst1 == ofst2) {
        puts("They are the same.");
    } else {
        puts("They are not the same.");
    }

    int fd1flags = fcntl(fd1, F_GETFL);
    if (fd1flags == -1) {
        perror("fcntl");
        exit(EXIT_FAILURE);
    }

    int fd2flags = fcntl(fd2, F_GETFL);
    if (fd2flags == -1) {
        perror("fcntl");
        exit(EXIT_FAILURE);
    }

    printf("fd1flags = %d VS fd2flags = %d\n", fd1flags, fd2flags);

    if (fd1flags == fd2flags) {
        puts("They are the same.");
    } else {
        puts("They are not the same.");
    }
}
