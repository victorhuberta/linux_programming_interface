#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    if (argc < 3 || strcmp(argv[1], "--help") == 0) {
        puts("Usage: ./atomic_append file num_bytes [x]");
        exit(EXIT_FAILURE);
    }

    char *filename = argv[1];
    long num_bytes = atol(argv[2]);
    int omit_append = 0;

    if (argc > 3) {
        if (strcmp(argv[3], "x") == 0)
            omit_append = 1;
    }

    int fd = -1;
    if (omit_append) {
        fd = open(filename, O_WRONLY | O_CREAT, 0666);
    } else {
        fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, 0666);
    }

    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < num_bytes; ++i) {
        if (omit_append) {
            if (lseek(fd, 0, SEEK_END) == -1) {
                perror("lseek");
                exit(EXIT_FAILURE);
            }
        }

        if (write(fd, "a", 1) == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }
    }

    if (close(fd) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}
