#!/bin/bash
# Test the functionality of fstest module.

test_dir="testdir/"

# Create files in random order.
cmd1="./$1 -r -n 20000 $test_dir"

# Create files in sequential order.
cmd2="./$1 -s -n 20000 $test_dir"

function test {
    mkdir -p "$test_dir"
    $1
    rm -rf "$test_dir" 
}

test "$cmd1"
test "$cmd2"
