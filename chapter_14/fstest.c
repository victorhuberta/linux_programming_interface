/**
 * Module: fstest
 * Author: Victor Huberta
 *
 * # Description
 * Measures the time required to create and then remove a large
 * number of 1-byte files from a single directory for the specified
 * file system.
 */

#define _XOPEN_SOURCE 500

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <fcntl.h>

#define N_FILES_MAX 1000000
#define N_FILENAME_LEN 8
#define N_BUCKET_BITS (sizeof *tbuckets->buckets * 8)

typedef struct Files {
    char **filenames;
    int n_files;
} Files;

typedef struct TrackerBuckets {
    int *buckets;
    int n_buckets;
} TrackerBuckets;

void usage_err(char *prog_name, char *msg);
char *trslash(char *path);
Files *mkfiles(int n_files);
TrackerBuckets *mktbuckets(int n_files);
double control_files(void (*ctlfunc)(void *), char *dirname);
double control_files_rn(void (*ctlfunc)(void *), char *dirname);
int find_valid_r(int r);
int at_bplimit(int bcktpos, int bitpos, int bplimit);
int was_encountered(int bucket, int bitpos);
int mark_bucket(int bucket, int bitpos);
char *conspath(char *dirpath, char *filename);
void create_file(void *fpath);
void remove_file(void *fpath);
double msrtime(void (*func)(void *), void *arg);
void free_files(Files *files);
void free_str_array(char **str_array, int n);
void free_tbuckets(TrackerBuckets *tbuckets);
void on_err_exit(char *fname, int fres);
#define on_ptr_err_exit(fname, fres) on_err_exit(fname, (fres) ? 0 : -1)

static Files *files;
static TrackerBuckets *tbuckets;

int main(int argc, char **argv)
{
    int seq = 0, opt;
    long n_files = 0;
    char *endptr, *dirname;
    double create_time, remove_time;

    if (argc < 2 || strcmp(argv[1], "--help") == 0)
        usage_err(argv[0], NULL);

    while ((opt = getopt(argc, argv, "srn:")) != -1) {
        switch (opt) {
            case 's': seq = 1; break;
            case 'r': seq = 0; break;
            case 'n':
                n_files = strtol(optarg, &endptr, 10);
                if (*optarg == '\0' || *endptr != '\0')
                    usage_err(argv[0], "Invalid number of files given.");
                break;
            default:
                usage_err(argv[0], "Invalid option(s) given.");
                break;
        }
    }

    if (n_files <= 0 || n_files > N_FILES_MAX)
        usage_err(argv[0], "Please specify the valid number of files.");

    if (optind >= argc)
        usage_err(argv[0], "Invalid number of arguments.");

    dirname = trslash(argv[optind]);
    files = mkfiles(n_files);
    tbuckets = mktbuckets(n_files);

    if (seq == 1)
        create_time = control_files(create_file, dirname);
    else
        create_time = control_files_rn(create_file, dirname);

    remove_time = control_files(remove_file, dirname);

    free_files(files);
    free_tbuckets(tbuckets);

    printf("Create time: %.3f ms\n", create_time);
    printf("Remove time: %.3f ms\n", remove_time);

    exit(EXIT_SUCCESS);
}

void usage_err(char *prog_name, char *msg)
{
    if (msg != NULL)
        fprintf(stderr, "%s\n", msg);

    fprintf(stderr, "Usage: %s [options] dir\n\n", prog_name);
    fprintf(stderr, "Available options:\n");
#define fpe(str) fprintf(stderr, "  " str)
    fpe("-s             [create files sequentially in ascending order]\n");
    fpe("-r             [create files in random order (default)]\n");
    fpe("-n files_num   [number of files, e.g., '10000' or '20000']\n");
    fprintf(stderr, "       MAX files_num is %d\n", N_FILES_MAX);

    exit(EXIT_FAILURE);
}

char *trslash(char *path)
{
    int i;

    for (i = strlen(path) - 1; path[i] == '/'; --i)
        path[i] = '\0';

    return path;
}

Files *mkfiles(int n_files)
{
    int i;
    char *filename, **filenames;
    Files *files = malloc(sizeof *files);
    on_ptr_err_exit("malloc", files);
    
    filenames = malloc(n_files * sizeof *filenames);
    on_ptr_err_exit("malloc", filenames);

    for (i = 0; i < n_files; ++i) {
        filename = malloc(N_FILENAME_LEN * sizeof *filename);
        on_ptr_err_exit("malloc", filename);

        snprintf(filename, N_FILENAME_LEN, "x%06d", i);
        filenames[i] = filename;
    }

    files->filenames = filenames;
    files->n_files = n_files;

    return files;
}

TrackerBuckets *mktbuckets(int n_files)
{
    int n_buckets;
    TrackerBuckets *tbuckets = malloc(sizeof *tbuckets);
    on_ptr_err_exit("malloc", tbuckets);

    n_buckets = n_files / N_BUCKET_BITS;
    n_buckets = ((n_files % N_BUCKET_BITS) == 0) ? n_buckets : n_buckets + 1;

    tbuckets->buckets = calloc(n_buckets, N_BUCKET_BITS);
    on_ptr_err_exit("calloc", tbuckets->buckets);

    tbuckets->n_buckets = n_buckets;

    return tbuckets;
}

double control_files(void (*ctlfunc)(void *), char *dirname)
{
    char *fpath;
    double elapsed_tot = 0.0;
    int i;

    for (i = 0; i < files->n_files; ++i) {
        fpath = conspath(dirname, files->filenames[i]);
        elapsed_tot += msrtime(ctlfunc, fpath);
        free(fpath);
    }

    return elapsed_tot;
}

double control_files_rn(void (*ctlfunc)(void *), char *dirname)
{
    char *fpath;
    double elapsed_tot = 0.0;
    int i, r;

    srand(time(NULL));

    for (i = 0; i < files->n_files; ++i) {
        r = find_valid_r(rand() % files->n_files);
        if (files->filenames[r] == NULL) {
            printf("%d\n", r);
        }
        fpath = conspath(dirname, files->filenames[r]);
        elapsed_tot += msrtime(ctlfunc, fpath);
        free(fpath);
    }

    return elapsed_tot;
}

int find_valid_r(int r)
{
    int bucket, bcktpos, bitpos, bplimit;

    bplimit = files->n_files % N_BUCKET_BITS;
    bplimit = (bplimit == 0) ? N_BUCKET_BITS : bplimit;

    bcktpos = r / N_BUCKET_BITS;
    bitpos = r % N_BUCKET_BITS;
    bucket = tbuckets->buckets[bcktpos];

    while (was_encountered(bucket, bitpos))  {
        ++bitpos;
        if (bitpos >= N_BUCKET_BITS || at_bplimit(bcktpos, bitpos, bplimit)) {
            ++bcktpos;
            bitpos = 0;
        }
        if (bcktpos >= tbuckets->n_buckets) bcktpos = 0;

        bucket = tbuckets->buckets[bcktpos];
    } 

    tbuckets->buckets[bcktpos] = mark_bucket(bucket, bitpos);

    return (bcktpos * N_BUCKET_BITS) + bitpos; /* valid r */
}

int at_bplimit(int bcktpos, int bitpos, int bplimit)
{
    return (bcktpos == tbuckets->n_buckets - 1) && (bitpos >= bplimit);
}

int was_encountered(int bucket, int bitpos)
{
    int bit = (int) pow(2, bitpos);

    bucket &= bit;

    return (bucket != 0);
}

int mark_bucket(int bucket, int bitpos)
{
    int bit = (int) pow(2, bitpos);

    bucket |= bit;

    return bucket;
}

char *conspath(char *dirpath, char *filename)
{
    int n_fpath_len; 
    char *fpath; 

    n_fpath_len = strlen(dirpath) + N_FILENAME_LEN + 1;
    fpath = malloc(n_fpath_len * sizeof *fpath);
    on_ptr_err_exit("malloc", fpath);

    snprintf(fpath, n_fpath_len, "%s/%s", dirpath, filename);

    return fpath;
}

void create_file(void *fpath)
{
    int fd = open((char *) fpath, O_RDONLY | O_CREAT, 0644);
    on_err_exit("open", fd);
    on_err_exit("close", close(fd));
}

void remove_file(void *fpath)
{
    on_err_exit("unlink", unlink((char *) fpath));
}

double msrtime(void (*func)(void *), void *arg)
{
    struct timeval tv1, tv2;
    double elapsed;

    if (gettimeofday(&tv1, NULL) == -1) return -1;
    (*func)(arg);
    if (gettimeofday(&tv2, NULL) == -1) return -1;

    elapsed = (tv2.tv_sec - tv1.tv_sec) * 1000.0; /* delta in secs */
    elapsed += (tv2.tv_usec - tv1.tv_usec) / 1000.0; /* delta in usecs */

    return elapsed; /* delta in msecs */
}

void free_files(Files *files)
{
    free_str_array(files->filenames, files->n_files);
    free(files);
}

void free_str_array(char **str_array, int n)
{
    int i;

    for (i = 0; i < n; ++i)
        free(str_array[i]);

    free(str_array);
}

void free_tbuckets(TrackerBuckets *tbuckets)
{
    free(tbuckets->buckets);
    free(tbuckets);
}

void on_err_exit(char *fname, int fres)
{
    if (fres == -1) {
        perror(fname);
        exit(EXIT_FAILURE);
    }
}
