#define _BSD_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

int chmodarX(const char *pathname);

int main(int argc, char **argv)
{
    int i;

    if (argc < 2 || ! strcmp(argv[1], "--help")) {
        printf("Usage: %s pathname...\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    for (i = 1; i < argc; ++i) {
        if (chmodarX(argv[i]) == -1) {
            perror("chmodarX");
            exit(EXIT_FAILURE);
        }
    }

    exit(EXIT_SUCCESS);
}

#define ALL_READ_PERM (S_IRUSR | S_IRGRP | S_IROTH)
#define ALL_EXEC_PERM (S_IXUSR | S_IXGRP | S_IXOTH)

int chmodarX(const char *pathname)
{
    struct stat st;
    mode_t perm = ALL_READ_PERM;

    if (stat(pathname, &st) == -1)
        return -1;

    if ((st.st_mode & S_IFMT) == S_IFDIR ||
        (st.st_mode & S_IXUSR) != 0 ||
        (st.st_mode & S_IXGRP) != 0 ||
        (st.st_mode & S_IXOTH) != 0) {

        perm |= ALL_EXEC_PERM;
    }

    return chmod(pathname, perm);
}
