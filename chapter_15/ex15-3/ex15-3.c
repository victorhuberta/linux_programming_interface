#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>

#define FPATH "test.txt"

int main(int argc, char **argv)
{
    struct stat st;
    long timestamp; 

    if (stat(FPATH, &st) == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    timestamp = st.st_atim.tv_sec * pow(10, 9) + st.st_atim.tv_nsec;
    printf("File's last access timestamp = %ld\n", (long) timestamp);

    timestamp = st.st_mtim.tv_sec * pow(10, 9) + st.st_mtim.tv_nsec;
    printf("File's last modification timestamp = %ld\n", (long) timestamp);

    timestamp = st.st_ctim.tv_sec * pow(10, 9) + st.st_ctim.tv_nsec;
    printf("File's last status change timestamp = %ld\n", (long) timestamp);

    exit(EXIT_SUCCESS);
}
