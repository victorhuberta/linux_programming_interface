#define _XOPEN_SOURCE 500

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define NEW_PERM (S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)
#define NEW_UMASK (S_IXGRP | S_IXOTH)

int main(int argc, char **argv)
{
    int fd;
    char *fpath;

    if (argc < 2 || strcmp(argv[1], "--help") == 0) {
        printf("Usage: %s path\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    fpath = argv[1];

    umask(NEW_UMASK);

    if ((fd = open(fpath, O_CREAT, 0777)) == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    if (fchmod(fd, NEW_PERM) == -1) {
        perror("chmod");
        exit(EXIT_FAILURE);
    }

    if (close(fd) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }

    if (open(fpath, O_RDONLY) == -1)
        printf("Just as expected, can't read the file.\n");

    exit(EXIT_SUCCESS);
}
