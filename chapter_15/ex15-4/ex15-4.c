#define _XOPEN_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

void usage_err(char *prog, char *msg);
int vhz_eaccess(const char *pathname, int mode);

int main(int argc, char **argv)
{
    int opt, res, chosen;
    char *pathname;

    while ((opt = getopt(argc, argv, "frwx")) != -1) {
        switch (opt) {
            case 'f': chosen = F_OK; break;
            case 'r': chosen = R_OK; break;
            case 'w': chosen = W_OK; break;
            case 'x': chosen = X_OK; break;
            default: usage_err(argv[0], "Invalid option(s) given.");
        }
    }

    if (optind >= argc)
        usage_err(argv[0], "Too many arguments.");

    pathname = argv[optind];

    res = vhz_eaccess(pathname, chosen);

    switch (chosen) {
        case F_OK:
            printf((res == -1) ?
                "%s does not exist.\n" :
                "%s exists.\n", pathname);
            break;
        case R_OK:
            printf((res == -1) ?
                "%s can't be read by this user.\n" :
                "%s can be read by this user.\n", pathname);
            break;
        case W_OK:
            printf((res == -1) ?
                "%s can't be written by this user.\n" :
                "%s can be written by this user.\n", pathname);
            break;
        case X_OK:
            printf((res == -1) ?
                "%s can't be executed by this user.\n" :
                "%s can be executed by this user.\n", pathname);
            break;
        default:
            printf("Invalid mode given.\n");
            break;
    }

    exit(EXIT_SUCCESS);
}

void usage_err(char *prog, char *msg)
{
    if (msg != NULL)
        fprintf(stderr, "%s\n", msg);

    fprintf(stderr, "Usage: %s -[frwx] pathname\n", prog);

    exit(EXIT_FAILURE);
}

/* Vehazet's version of eaccess / euidaccess. */
int vhz_eaccess(const char *pathname, int mode)
{
    struct stat st;

    if (stat(pathname, &st) == -1)
        return -1;

    if (st.st_uid != geteuid()) {
        errno = EACCES;
        return -1;
    }

    switch (mode) {
        case F_OK: return 0;
        case R_OK: return (st.st_mode & S_IRUSR) ? 0 : -1;
        case W_OK: return (st.st_mode & S_IWUSR) ? 0 : -1;
        case X_OK: return (st.st_mode & S_IXUSR) ? 0 : -1;
        default:
            errno = EINVAL;
            return -1;
    }
}
