#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/fs.h>
#include <sys/ioctl.h>

#define ATTR_ADD 1
#define ATTR_REM 2
#define ATTR_EQU 3
int chattr(int curattr, char attrc, int action);

int main(int argc, char **argv)
{
    char *pathname, *mode, attrc;
    int i, fd, curattr, action;

    if (argc != 3 || strcmp(argv[0], "--help") == 0) {
        printf("Usage: %s mode pathname\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    mode = argv[1];
    pathname = argv[2];

    if ((fd = open(pathname, O_RDWR)) == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    if (ioctl(fd, FS_IOC_GETFLAGS, &curattr) == -1) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }

    for (i = 0; mode[i] != '\0'; ++i) {
        switch (mode[i]) {
            case '+':
                action = ATTR_ADD;
                break;
            case '-':
                action = ATTR_REM;
                break;
            case '=':
                curattr = 0;
                action = ATTR_ADD;
                break;
            default:
                attrc = mode[i];
                if ((curattr = chattr(curattr, attrc, action)) == -1) {
                    perror("chattr");
                    exit(EXIT_FAILURE);
                }
                break;
        }
    }

    if (ioctl(fd, FS_IOC_SETFLAGS, &curattr) == -1) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}

int chattr(int curattr, char attrc, int action)
{
    int newattr;

    switch (attrc) {
        case 'a': newattr = FS_APPEND_FL; break;
        case 'c': newattr = FS_COMPR_FL; break;
        case 'D': newattr = FS_DIRSYNC_FL; break;
        case 'i': newattr = FS_IMMUTABLE_FL; break;
        case 'j': newattr = FS_JOURNAL_DATA_FL; break;
        case 'A': newattr = FS_NOATIME_FL; break;
        case 'd': newattr = FS_NODUMP_FL; break;
        case 't': newattr = FS_NOTAIL_FL; break;
        case 's': newattr = FS_SECRM_FL; break;
        case 'S': newattr = FS_SYNC_FL; break;
        case 'T': newattr = FS_TOPDIR_FL; break;
        case 'u': newattr = FS_UNRM_FL; break;
        default:
            errno = EINVAL;
            return -1;
    }
    switch (action) {
        case ATTR_ADD:
            curattr |= newattr;
            break;
        case ATTR_REM:
            curattr &= ~newattr;
            break;
        default:
            errno = EINVAL;
            return -1;
    }

    return curattr;
}
