#include <stdlib.h>
#include <setjmp.h>
#include <stdio.h>

jmp_buf env;

void function1() {
    char hello[] = "hello";

    switch (setjmp(env)) {
    case 0:
        puts("Running inside function1 for the first time.");
        printf("0x");
        for (int i = 0; i < 5; ++i)
            printf("%x", hello[i]);
	printf("\n");
        break;
    case 1:
        puts("Running inside function1 for the second time.");
        printf("0x");
        for (int i = 0; i < 5; ++i)
            printf("%x", hello[i]);
	printf("\n");
        break;
    }
}

void function2() {
    puts("Running inside function2.");
    longjmp(env, 1);
}

int main() {
    function1();
    function2();
    exit(EXIT_SUCCESS);
}
