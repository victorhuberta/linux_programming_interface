#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

void print_environ();
int setenv(const char *name, const char *value, int overwrite);
int unsetenv(const char *name);

extern char **environ;

int main()
{
    print_environ();

    if (setenv("FOO", "BAR", 0) == -1) {
        perror("setenv");
        exit(EXIT_FAILURE);
    }

    print_environ();

    if (setenv("FOO", "BAZ", 1) == -1) {
        perror("setenv");
        exit(EXIT_FAILURE);
    }

    print_environ();

    if (setenv("FOO", "BARZ", 0) == -1) {
        perror("setenv");
        exit(EXIT_FAILURE);
    }

    print_environ();

    if (unsetenv("FOO") == -1) {
        perror("unsetenv");
        exit(EXIT_FAILURE);
    }

    print_environ();

    exit(EXIT_SUCCESS);
}

void print_environ()
{
    char **p;

    puts("=================================");

    for (p = environ; *p != NULL; ++p)
        puts(*p);

    puts("=================================");
}

int setenv(const char *name, const char *value, int overwrite)
{
    int new_env_len = strlen(name) + strlen(value) + 2;

    static char *new_env;
    new_env = malloc(new_env_len);

    strcpy(new_env, name);
    new_env[strlen(name)] = '=';
    strcat(new_env, value);

    if (getenv(name) == NULL || overwrite == 1) {
        if (putenv(new_env) != 0) {
            errno = ENOMEM;
            return -1;
        }
    }

    return 0;
}

int unsetenv(const char *name)
{
    char **p;
    int len;
    char *env_name;

    for (p = environ; *p != NULL; ++p) {
        len = strcspn(*p, "=");
        env_name = malloc(len + 1);
        if (env_name == NULL)
            return -1;

        strncpy(env_name, *p, len);

        if (strcmp(env_name, name) == 0) {
            char **tmp;
            for (tmp = p; *tmp != NULL; ++tmp) {
                *tmp = *(tmp + 1);
            }
        }

        free(env_name);
    }

    return 0;
}
