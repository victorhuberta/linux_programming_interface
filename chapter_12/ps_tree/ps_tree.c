#define _XOPEN_SOURCE 500

#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

struct process {
    pid_t pid;
    char *name;
    struct process *parent;
    struct process *peer;
    struct process *child;
};

#define MAX_PATH_LEN pathconf("/proc", _PC_PATH_MAX)
#define ALLOC_NEW_PROCESS() (struct process *) malloc(sizeof(struct process))

#define PSTATUS_PATH_FMT "/proc/%s/status"
#define PSTATUS_SCAN_FMT "%s\t%[^\n]\n"
#define MAX_PNAME_LEN 60
#define MAX_ATTR_LEN 50
#define MAX_LINE_LEN 256

void get_root_pname(struct process *root);
void find_children(struct process *proc, char *proc_path);
void add_new_peer(struct process *child, struct process *new_peer);
void print_process(struct process *proc, int type);

int main(void)
{
    struct process *root;
    char *proc_path;

    root = ALLOC_NEW_PROCESS();
    if (root == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    memset(root, 0, sizeof(struct process));
    root->pid = 1;
    root->name = malloc(MAX_PNAME_LEN);
    get_root_pname(root);

    proc_path = malloc(MAX_PATH_LEN);
    if (proc_path == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    find_children(root, proc_path);

    print_process(root, 1);
    puts("");

    exit(EXIT_SUCCESS);
}

void get_root_pname(struct process *root)
{
    FILE *rsts_file;
    char sts_attr[MAX_ATTR_LEN] = {0};
    char line[MAX_LINE_LEN] = {0};

    rsts_file = fopen("/proc/1/status", "r");
    if (rsts_file == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    while (fscanf(rsts_file, PSTATUS_SCAN_FMT, sts_attr, line) != EOF) {
        if (strcmp(sts_attr, "Name:") == 0) {
            sscanf(line, "%s", root->name);
            break;
        }
    }

    fclose(rsts_file);
}

void find_children(struct process *proc, char *proc_path)
{
    DIR *dirp;
    static struct dirent *entry, *result;
    char *endptr;
    FILE *sts_file;
    char sts_attr[MAX_ATTR_LEN] = {0};
    char line[MAX_LINE_LEN] = {0};
    struct process *new_child, *peer;
    pid_t ppid;

    if (entry == NULL)
        entry = (struct dirent *) malloc(sizeof(struct dirent));

    memset(proc_path, '\0', strlen(proc_path));

    dirp = opendir("/proc");
    if (dirp == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    while (readdir_r(dirp, entry, &result) == 0 && result != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        if (strtol(entry->d_name, &endptr, 10) == 1)
            continue;

        if (*endptr != '\0')
            continue;

        if (snprintf(proc_path, MAX_PATH_LEN,
                PSTATUS_PATH_FMT, entry->d_name) < 0)
            continue;

        sts_file = fopen(proc_path, "r");
        if (sts_file == NULL)
            continue;

        new_child = ALLOC_NEW_PROCESS();
        memset(new_child, 0, sizeof(struct process));
        new_child->name = malloc(MAX_PNAME_LEN);

        while (fscanf(sts_file, PSTATUS_SCAN_FMT, sts_attr, line) != EOF) {
            if (strcmp(sts_attr, "PPid:") == 0) {
                sscanf(line, "%ld", (long *) &ppid);

                if (ppid == 0)
                    break;

                if (ppid == proc->pid) {
                    new_child->parent = proc;

                    if (proc->child == NULL)
                        proc->child = new_child;
                    else
                        add_new_peer(proc->child, new_child);
                }

                if (new_child->pid != 0)
                    break;
            } else if (strcmp(sts_attr, "Pid:") == 0) {
                sscanf(line, "%ld", (long *) &new_child->pid);
            } else if (strcmp(sts_attr, "Name:") == 0) {
                sscanf(line, "%s", new_child->name);
            }
        }
        fclose(sts_file);
    }

    closedir(dirp);

    peer = proc->child;
    while (peer != NULL) {
        find_children(peer, proc_path);
        peer = peer->peer;
    }
}

void add_new_peer(struct process *child, struct process *new_peer)
{
    if (child->peer == NULL)
        child->peer = new_peer;
    else
        add_new_peer(child->peer, new_peer);
}

void print_process(struct process *proc, int type)
{
    if (proc == NULL) {
        printf(" X ");
        return;
    }

    if (type)
        printf("< %s < ", proc->name);
    else
        printf("\n< %s < - %s -", proc->parent->name, proc->name);

    print_process(proc->child, 1);
    print_process(proc->peer, 0);
}
