/*
 * Module: usrproc; Author: Victor Huberta
 *
 * # Description
 * List the pid and name of every process which
 * was started by the specified user.
 *
 */

#include "usrproc.h"

int main(int argc, char *argv[])
{
    char *uname, *proc_path;
    uid_t uid;

    if (argc < 2 || strcmp(argv[1], "--help") == 0) {
        printf("Usage: %s username\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    uname = argv[1];
    uid = get_uid(argv[0], uname);

    proc_path = malloc(MAX_PATH_LEN);
    if (proc_path == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    memset(proc_path, '\0', MAX_PATH_LEN);

    errno = 0;
    find_user_processes(proc_path, uid);

    if (errno != 0) {
        perror("error");
        exit(EXIT_FAILURE);
    }

    free(proc_path);
    exit(EXIT_SUCCESS);
}

/*
 * Act as an error handling wrapper to the
 * underlying user_id_from_name call.
 */
uid_t get_uid(const char *program, const char *name)
{
    uid_t uid;

    uid = user_id_from_name(name);
    if (uid == -1) {
        if (errno != 0)
            perror("user_id_from_name");
        else
            printf("%s: user %s not found\n", program, name);

        exit(EXIT_FAILURE);
    }

    return uid;
}

uid_t user_id_from_name(const char *name)
{
    struct passwd *pwd;

    errno = 0;
    pwd = getpwnam(name);

    return (pwd == NULL) ? -1 : pwd->pw_uid;
}

/*
 * Find all processes that belong to the given user.
 */
int find_user_processes(char *proc_path, uid_t uid)
{
    DIR *dirp;
    struct dirent *entry;
    char *endptr;

    dirp = opendir("/proc");
    if (dirp == NULL)
        return -1;

    entry = (struct dirent *) dirp;

    while ((entry = readdir(dirp)) != NULL) {
        /* Ignore the current and parent directories */
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        /* A proc dir has its name in integer */
        strtol(entry->d_name, &endptr, 10);
        if (*endptr != '\0')
            continue;

        /* Reset proc_path with a new dir name */
        memset(proc_path, '\0', strlen(proc_path));
        if (snprintf(proc_path, MAX_PATH_LEN, PROC_DIR_FMT, entry->d_name) < 0)
            break;

        if (! is_dir(proc_path))
            continue;

        strcat(proc_path, "/status");

        if (print_if_started_by_usr(proc_path, uid) == -1)
            break;
    }

    return closedir(dirp);
}

int is_dir(const char *proc_path)
{
    DIR *proc_dirp;

    proc_dirp = opendir(proc_path);
    if (proc_dirp == NULL) {
        return 0; /* Ignore any errors and assume errno == ENOTDIR */
    } else {
        closedir(proc_dirp); /* Ignore any errors */
        return 1;
    }
}

/*
 * Print process' id and process' name if process
 * belongs to the user.
 */
int print_if_started_by_usr(char *proc_path, uid_t uid)
{
    int started_by_usr = 0;
    char proc_name[MAX_ATTR_LEN] = {0};
    char sts_attr[MAX_ATTR_LEN] = {0};
    char line[MAX_LINE_LEN] = {0};
    FILE *sts_file;
    uid_t proc_uid;
    pid_t pid;

    sts_file = fopen(proc_path, "r");
    if (sts_file == NULL)
        return -1;

    while (fscanf(sts_file, "%s\t%[^\n]\n", sts_attr, line) != EOF) {
        if (strcmp(sts_attr, "Name:") == 0)
        {
            strncpy(proc_name, line, MAX_ATTR_LEN);
        }
        else if (strcmp(sts_attr, "Pid:") == 0)
        {
            sscanf(line, "%ld", (long *) &pid);
        }
        else if (strcmp(sts_attr, "Uid:") == 0)
        {
            sscanf(line, "%ld", (long *) &proc_uid);
            if (proc_uid == uid)
                started_by_usr = 1;
            else
                break;
        }
    }

    if (started_by_usr)
        printf("Pid: %ld\tName: %s\n", (long) pid, proc_name);

    return (fclose(sts_file) == EOF) ? -1 : 0;
}
