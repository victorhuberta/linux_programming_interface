#define _XOPEN_SOURCE 500

#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>

#define MAX_PATH_LEN pathconf("/proc", _PC_PATH_MAX)

uid_t get_uid(const char *program, const char *name);
uid_t user_id_from_name(const char *name);

#define PROC_DIR_FMT "/proc/%s"
int find_user_processes(char *proc_path, uid_t uid);

int is_dir(const char *proc_path);

#define MAX_ATTR_LEN 50
#define MAX_LINE_LEN 256
int print_if_started_by_usr(char *proc_path, uid_t uid);
