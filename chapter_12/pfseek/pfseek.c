#define _XOPEN_SOURCE 500

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <errno.h>

#define FD_PATH_FMT "/proc/%s/fd"
#define FD_CONTENT_PATH_FMT "%s/%s"
#define FD_PATH_LEN pathconf(FD_PATH_FMT, _PC_PATH_MAX)

void print_proc_name(pid_t pid);

int main(int argc, char *argv[])
{
    DIR *dirp, *fdirp;
    struct dirent *entry;
    pid_t pid;
    char *endptr, *fd_path, *link_path, *fd_content_path, *substr;
    ssize_t lpath_len;

    if (argc < 2 || strcmp(argv[1], "--help") == 0) {
        printf("Usage: %s file-path\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    substr = argv[1];

    fd_path = malloc(FD_PATH_LEN);
    if (fd_path == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    link_path = malloc(FD_PATH_LEN);
    if (link_path == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    fd_content_path = malloc(FD_PATH_LEN);
    if (fd_content_path == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    dirp = opendir("/proc");
    if (dirp == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    entry = (struct dirent *) dirp;

    errno = 0;
    while ((entry = readdir(dirp)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        pid = (pid_t) strtol(entry->d_name, &endptr, 10);
        if (*endptr != '\0')
            continue;

        memset(fd_path, '\0', FD_PATH_LEN);
        snprintf(fd_path, FD_PATH_LEN, FD_PATH_FMT, entry->d_name);

        fdirp = opendir(fd_path);
        if (fdirp == NULL)
            break;

        while ((entry = readdir(fdirp)) != NULL) {
            if (strcmp(entry->d_name, ".") == 0
                || strcmp(entry->d_name, "..") == 0)
                continue;

            strtol(entry->d_name, &endptr, 10);
            if (*endptr != '\0')
                continue;

            snprintf(fd_content_path, FD_PATH_LEN,
                FD_CONTENT_PATH_FMT, fd_path, entry->d_name);

            lpath_len = readlink(fd_content_path, link_path, FD_PATH_LEN);
            if (lpath_len == -1) {
                perror("readlink");
                continue;
            }

            link_path[lpath_len] = '\0';

            if (strstr(link_path, substr) != NULL) {
                print_proc_name(pid);
                printf("\t PID: %ld", (long) pid);
                printf(" -> %s\n", link_path);
            }

        }
    }

    closedir(dirp);
    free(fd_path);
    free(link_path);

    if (errno != 0) {
        perror("error");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}

#define MAX_ATTR_LEN 50
#define MAX_LINE_LEN 256

void print_proc_name(pid_t pid)
{
    char *proc_path;
    char proc_name[MAX_ATTR_LEN] = {0};
    char sts_attr[MAX_ATTR_LEN] = {0};
    char line[MAX_LINE_LEN] = {0};
    FILE *sts_file;

    proc_path = malloc(FD_PATH_LEN);
    if (proc_path == NULL) {
        perror("malloc");
        return;
    }

    snprintf(proc_path, FD_PATH_LEN, "/proc/%ld/status", (long) pid);

    sts_file = fopen(proc_path, "r");
    if (sts_file == NULL)
        return;

    while (fscanf(sts_file, "%s\t%[^\n]\n", sts_attr, line) != EOF) {
        if (strcmp(sts_attr, "Name:") == 0) {
            strncpy(proc_name, line, MAX_ATTR_LEN);
            printf("Name: %s", proc_name);
            break;
        }
    }

    free(proc_path);
    fclose(sts_file);
}
