#define _GNU_SOURCE

#include <sys/utsname.h>
#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    struct utsname uts;

    if (uname(&uts) == -1) {
        perror("uname");
        exit(EXIT_FAILURE);
    }

    printf("Node name: %s\n", uts.nodename);
    printf("System name: %s\n", uts.sysname);
    printf("Release: %s\n", uts.release);
    printf("Version: %s\n", uts.version);
    printf("Machine: %s\n", uts.machine);
    printf("Domain name: %s\n", uts.domainname);

    exit(EXIT_SUCCESS);
}
