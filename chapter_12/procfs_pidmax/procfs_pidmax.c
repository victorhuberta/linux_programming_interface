#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MAX_PID_LEN 50

int main(int argc, char *argv[])
{
    int fd, replace;
    ssize_t b;
    char old_max[MAX_PID_LEN], *new_max, *endptr;

    if (argc > 1) {
        strtol(argv[1], &endptr, 10);
        if (*argv[1] == '\0' || *endptr != '\0') {
            printf("%s is not a valid number\n", argv[1]);
            exit(EXIT_FAILURE);
        }

        new_max = argv[1];
        replace = 1;
    }

    fd = open("/proc/sys/kernel/pid_max", O_RDWR);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    b = read(fd, old_max, MAX_PID_LEN);
    if (b == -1) {
        perror("read");
        exit(EXIT_FAILURE);
    }

    printf("Old value: %s\n", old_max);

    if (replace) {
        b = write(fd, new_max, strlen(new_max));
        if (b == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }

        printf("/proc/sys/kernel/pid_max now contains %s\n", new_max);
    }

    exit(EXIT_SUCCESS);
}
