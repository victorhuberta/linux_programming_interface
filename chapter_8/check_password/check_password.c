#define _XOPEN_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <shadow.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define MAX_USR_LEN sysconf(_SC_LOGIN_NAME_MAX)

int main(void)
{
    struct passwd *pwd;
    struct spwd *spwd;

    char *username, *password, *encrypted, *p;
    size_t usr_len;

    short auth_ok;

    username = malloc(MAX_USR_LEN);
    if (username == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    printf("Username: ");

    if (fgets(username, MAX_USR_LEN, stdin) == NULL)
        exit(EXIT_FAILURE);

    usr_len = strlen(username);
    if (username[usr_len - 1] == '\n')
        username[usr_len - 1] = '\0';

    errno = 0;
    pwd = getpwnam(username);
    if (pwd == NULL) {
        if (errno != 0)
            perror("getpwnam");
        else
            printf("User %s was not found.\n", username);

        exit(EXIT_FAILURE);
    }

    spwd = getspnam(username);
    if (spwd == NULL) {
        if (errno == EACCES)
            puts("You need to run this program as root!");
        else
            puts("Details can't be found in the shadow file.");

        exit(EXIT_FAILURE);
    }

    pwd->pw_passwd = spwd->sp_pwdp;

    password = getpass("Password: ");

    encrypted = crypt(password, pwd->pw_passwd);

    /* clean password from memory */
    for (p = password; *p != '\0'; ++p)
        *p = '\0';

    if (encrypted == NULL) {
        perror("crypt");
        exit(EXIT_FAILURE);
    }

    auth_ok = strcmp(encrypted, pwd->pw_passwd) == 0;

    if (auth_ok)
        puts("You have been authenticated.");
    else
        puts("Wrong password entered.");

    exit(EXIT_SUCCESS);
}
