#define _XOPEN_SOURCE 500

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <pwd.h>
#include <string.h>

#define USERNAME "victorhuberta"

int main(void)
{
    struct passwd *pwd;

    errno = 0;
    pwd = getpwnam(USERNAME);
    if (pwd == NULL) {
        if (errno != 0)
            perror("getpwnam");
        else
            printf("User %s was not found.\n", USERNAME);

        exit(EXIT_FAILURE);
    }

    printf("Username: %s; UID: %ld\n", pwd->pw_name, (long) pwd->pw_uid);

    return 0;
}

struct passwd *getpwnam(const char *name)
{
    struct passwd *pwd;

    while ((pwd = getpwent()) != NULL) {
        if (strcmp(pwd->pw_name, name) == 0)
            return pwd;
    }

    endpwent();

    return NULL;
}
