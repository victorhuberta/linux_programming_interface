#define _DEFAULT_SOURCE

#include <sys/types.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>
#include <stdio.h>
#include <stdlib.h>

#define USERNAME "victorhuberta"
#define UID 1000
#define GROUPNAME "sudo"
#define GID 1001

char *usr_name_from_id(uid_t id);
uid_t usr_id_from_name(const char *name);
char *grp_name_from_id(gid_t id);
gid_t grp_id_from_name(const char *name);
void print_all_users(void);

int main(void)
{
    char *username;
    uid_t uid;
    char *grpname;
    gid_t gid;

    errno = 0;
    username = usr_name_from_id(UID);
    if (username == NULL) {
        if (errno != 0) {
            perror("usr_name_from_id");
            return -1;
        } else {
            printf("No user found by the id: %ld\n", (long) UID);
        }
    }

    errno = 0;
    uid = usr_id_from_name(USERNAME);
    if (uid == -1) {
        if (errno != 0) {
            perror("usr_id_from_name");
            return -1;
        } else {
            printf("No user found by the name: %s\n", USERNAME);
        }
    }

    errno = 0;
    grpname = grp_name_from_id(GID);
    if (username == NULL) {
        if (errno != 0) {
            perror("grp_name_from_id");
            return -1;
        } else {
            printf("No group found by the id: %ld\n", (long) GID);
        }
    }

    errno = 0;
    gid = grp_id_from_name(GROUPNAME);
    if (gid == -1) {
        if (errno != 0) {
            perror("grp_id_from_name");
            return -1;
        } else {
            printf("No group found by the name: %ld\n", (long) GROUPNAME);
        }
    }

    printf("user name: %s\n", username);
    printf("uid: %ld\n", (long) uid);
    printf("group name: %s\n", grpname);
    printf("gid: %ld\n", (long) gid);

    print_all_users();

    return 0;
}

char *usr_name_from_id(uid_t id)
{
    struct passwd *pwd;

    pwd = getpwuid(id);

    return (pwd == NULL) ? NULL : pwd->pw_name;
}

uid_t usr_id_from_name(const char *name)
{
    struct passwd *pwd;
    char *endptr;
    uid_t uid;

    if (name == NULL || *name == '\0')
        return -1;

    uid = (uid_t) strtol(name, &endptr, 10);

    if (*endptr == '\0')
        return uid;

    pwd = getpwnam(name);

    return (pwd == NULL) ? -1 : pwd->pw_uid;
}

char *grp_name_from_id(gid_t id)
{
    struct group *grp;

    grp = getgrgid(id);

    return (grp == NULL) ? NULL : grp->gr_name;
}

gid_t grp_id_from_name(const char *name)
{
    struct group *grp;
    char *endptr;
    gid_t gid;

    if (name == NULL || *name == '\0')
        return -1;

    gid = (gid_t) strtol(name, &endptr, 10);

    if (*endptr == '\0')
        return gid;

    grp = getgrnam(name);

    return (grp == NULL) ? -1 : grp->gr_gid;
}

void print_all_users(void)
{
    struct passwd *pwd;

    while ((pwd = getpwent()) != NULL)
        printf("username: %s; uid: %ld\n", pwd->pw_name, (long) pwd->pw_uid);

    endpwent();
}
