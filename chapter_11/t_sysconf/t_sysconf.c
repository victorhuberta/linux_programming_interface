#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>

void print_limit(const char *str, int name);

int main(void)
{
    print_limit("_SC_ARG_MAX", _SC_ARG_MAX);
    print_limit("_SC_LOGIN_NAME_MAX", _SC_LOGIN_NAME_MAX);
    print_limit("_SC_OPEN_MAX", _SC_OPEN_MAX);
    print_limit("_SC_NGROUPS_MAX", _SC_NGROUPS_MAX);
    print_limit("_SC_PAGESIZE", _SC_PAGESIZE);
    print_limit("_SC_RTSIG_MAX", _SC_RTSIG_MAX);

    exit(EXIT_SUCCESS);
}

void print_limit(const char *str, int name)
{
    long limit;

    errno = 0;
    limit = sysconf(name);
    if (limit == -1) {
        if (errno != 0) {
            perror("sysconf");
            exit(EXIT_FAILURE);
        } else {
            printf("%s: limit is indeterminate\n", str);
        }
    }

    printf("%s: %ld\n", str, limit);
}
