#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

void print_flimit(const char *str, int fd, int name);

int main(void)
{
    print_flimit("_PC_NAME_MAX", STDIN_FILENO, _PC_NAME_MAX);
    print_flimit("_PC_PATH_MAX", STDIN_FILENO, _PC_PATH_MAX);
    print_flimit("_PC_PIPE_BUF", STDIN_FILENO, _PC_PIPE_BUF);

    exit(EXIT_SUCCESS);
}

void print_flimit(const char *str, int fd, int name)
{
    long limit;

    errno = 0;
    limit = fpathconf(fd, name);
    if (limit == -1) {
        if (errno != 0) {
            perror("fpathconf");
            exit(EXIT_FAILURE);
        } else {
            printf("%s: limit is indeterminate\n", str);
        }
    }

    printf("%s: %ld\n", str, limit);
}
