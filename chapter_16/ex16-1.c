#define _XOPEN_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/xattr.h>

void usage_err(char *prog, char *msg);

int main(int argc, char **argv)
{
    int opt;
    char *name = NULL, *value = NULL, *pathname;

    while ((opt = getopt(argc, argv, "n:v:")) != -1) {
        switch (opt) {
            case 'n':
                name = optarg;
                break;
            case 'v':
                value = optarg;
                break;
            default:
                usage_err(argv[0], NULL);
                break;
        }
    }

    if (name == NULL || value == NULL)
        usage_err(argv[0], NULL);

    if (optind >= argc)
        usage_err(argv[0], "Too few arguments.");

    pathname = argv[optind];

    if (setxattr(pathname, name, value, strlen(value), 0) == -1) {
        perror("setxattr");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}

void usage_err(char *prog, char *msg)
{
    if (msg != NULL)
        fprintf(stderr, "%s\n", msg);

    fprintf(stderr, "Usage: %s -n name -v value file\n", prog);

    exit(EXIT_FAILURE);
}
