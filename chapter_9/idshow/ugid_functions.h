#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <stdlib.h>

char *usr_name_from_id(uid_t id);
uid_t usr_id_from_name(const char *name);
char *grp_name_from_id(gid_t id);
gid_t grp_id_from_name(const char *name);
