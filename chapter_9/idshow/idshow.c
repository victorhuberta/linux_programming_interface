#define _GNU_SOURCE

#include <unistd.h>
#include <limits.h>
#include <sys/fsuid.h>
#include <stdio.h>
#include "ugid_functions.h" /* usr_name_from_id() & grp_name_from_id() */

#define SG_SIZE (NGROUPS_MAX + 1)

int main(void)
{
    uid_t ruid, euid, suid, fsuid;
    gid_t rgid, egid, sgid, fsgid, glist[SG_SIZE];
    char *usr, *grp;
    int gc, i;

    if (getresuid(&ruid, &euid, &suid) == -1) {
        perror("getresuid");
        return 1;
    }

    if (getresgid(&rgid, &egid, &sgid) == -1) {
        perror("getresgid");
        return 1;
    }

    fsuid = setfsuid(0);
    fsgid = setfsgid(0);

    usr = usr_name_from_id(ruid);
    printf("rea: %ld; usr: %s\n", (long) ruid, (usr == NULL) ? "???" : usr);
    usr = usr_name_from_id(euid);
    printf("eff: %ld; usr: %s\n", (long) euid, (usr == NULL) ? "???" : usr);
    usr = usr_name_from_id(suid);
    printf("sav: %ld; usr: %s\n", (long) suid, (usr == NULL) ? "???" : usr);
    usr = usr_name_from_id(fsuid);
    printf("fis: %ld; usr: %s\n", (long) fsuid, (usr == NULL) ? "???" : usr);

    grp = grp_name_from_id(rgid);
    printf("rea: %ld; grp: %s\n", (long) rgid, (usr == NULL) ? "???" : grp);
    grp = grp_name_from_id(egid);
    printf("eff: %ld; grp: %s\n", (long) egid, (usr == NULL) ? "???" : grp);
    grp = grp_name_from_id(sgid);
    printf("sav: %ld; grp: %s\n", (long) sgid, (usr == NULL) ? "???" : grp);
    grp = grp_name_from_id(fsgid);
    printf("fis: %ld; grp: %s\n", (long) fsgid, (usr == NULL) ? "???" : grp);

    gc = getgroups(SG_SIZE, glist);
    if (gc == -1) {
        perror("getgroups");
        return 1;
    }

    printf("Total Supplementary Groups: %d\n", gc);
    for (i = 0; i < gc; ++i) {
        grp = grp_name_from_id(glist[i]);
        printf("id: %ld; grp: %s\n", (long) glist[i], (grp == NULL) ? "???" : grp);
    }

    return 0;
}
