#include "ugid_functions.h"

char *usr_name_from_id(uid_t id)
{
    struct passwd *pwd;

    pwd = getpwuid(id);

    return (pwd == NULL) ? NULL : pwd->pw_name;
}

uid_t usr_id_from_name(const char *name)
{
    struct passwd *pwd;
    char *endptr;
    uid_t uid;

    if (name == NULL || *name == '\0')
        return -1;

    uid = (uid_t) strtol(name, &endptr, 10);

    if (*endptr == '\0')
        return uid;

    pwd = getpwnam(name);

    return (pwd == NULL) ? -1 : pwd->pw_uid;
}

char *grp_name_from_id(gid_t id)
{
    struct group *grp;

    grp = getgrgid(id);

    return (grp == NULL) ? NULL : grp->gr_name;
}

gid_t grp_id_from_name(const char *name)
{
    struct group *grp;
    char *endptr;
    gid_t gid;

    if (name == NULL || *name == '\0')
        return -1;

    gid = (gid_t) strtol(name, &endptr, 10);

    if (*endptr == '\0')
        return gid;

    grp = getgrnam(name);

    return (grp == NULL) ? -1 : grp->gr_gid;
}
