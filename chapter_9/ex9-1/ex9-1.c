#define _GNU_SOURCE

#include <unistd.h>
#include <stdlib.h>
#include <sys/fsuid.h>
#include <stdio.h>

void print_uids(void);
void handle_err(const char *fname, const ssize_t res);

int main(void)
{
    print_uids();

    handle_err("setuid", setuid(2000));
    print_uids();

    handle_err("setreuid", setreuid(-1, 2000));
    print_uids();

    handle_err("seteuid", seteuid(2000));
    print_uids();

    handle_err("setfsuid", setfsuid(2000));
    print_uids();

    handle_err("setresuid", setresuid(-1, 2000, 3000));
    print_uids();

    exit(EXIT_SUCCESS);
}

void print_uids(void)
{
    uid_t ruid, euid, suid, fsuid;

    handle_err("getresuid", getresuid(&ruid, &euid, &suid));

    fsuid = setfsuid(0);

    printf("ruid: %ld\n", (long) ruid);
    printf("euid: %ld\n", (long) euid);
    printf("suid: %ld\n", (long) suid);
    printf("fsuid: %ld\n", (long) fsuid);
}

void handle_err(const char *fname, const ssize_t res)
{
    if (res == -1) {
        perror(fname);
        exit(EXIT_FAILURE);
    }
}
