#define _GNU_SOURCE

#include <unistd.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>

#define SG_SIZE (NGROUPS_MAX + 1)

void handle_err(const char *fname, const ssize_t res);
void print_glist(void);
int initgroups(const char *user, gid_t group);
char *grp_name_from_id(gid_t id);

int main(void)
{
    struct group *grp;

    print_glist();

    grp = getgrnam("mongodb");
    if (grp == NULL) {
        if (errno != 0)
            perror("getgrnam");
        else
            puts("Group mongodb not found.");
        exit(EXIT_FAILURE);
    }

    puts("Setting new group list...");
    initgroups("postgres", grp->gr_gid);

    print_glist();

    exit(EXIT_SUCCESS);
}

void handle_err(const char *fname, const ssize_t res)
{
    if (res == -1) {
        perror(fname);
        exit(EXIT_FAILURE);
    }
}

void print_glist(void)
{
    gid_t glist[SG_SIZE];
    int gc, i;
    char *grp_name;

    gc = getgroups(SG_SIZE, glist);
    handle_err("getgroups", gc);

    for (i = 0; i < gc; ++i) {
        grp_name = grp_name_from_id(glist[i]);
        printf("glist[%d]: %s (%ld)\n", i,
            (grp_name == NULL) ? "???" : grp_name, (long) glist[i]);
    }
}

int initgroups(const char *user, gid_t group)
{
    struct passwd *pwd;
    gid_t glist[SG_SIZE];
    int gc = SG_SIZE;

    if (user == NULL) return -1;

    pwd = getpwnam(user);
    if (pwd == NULL)
        return -1;

    handle_err("getgrouplist", getgrouplist(user, pwd->pw_gid, glist, &gc));

    if (gc < SG_SIZE)
        glist[gc] = group;

    handle_err("setgroups", setgroups(gc + 1, glist));

    return 0;
}

char *grp_name_from_id(gid_t id)
{
    struct group *grp;

    grp = getgrgid(id);

    return (grp == NULL) ? NULL : grp->gr_name;
}
