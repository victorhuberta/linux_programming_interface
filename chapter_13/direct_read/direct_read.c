/*
 * Module: direct_read; Author: Victor Huberta
 *
 * # Description
 * A simple example of the use of O_DIRECT while opening a file
 * for reading. This program takes up to four command-line arguments specifying,
 * in order, the file to be read, the number of bytes to be read from the file,
 * the offset to which the program should seek before reading from the file,
 * and the alignment of the data buffer passed to read(). The last two arguments
 * are optional, and default to offset 0 and 4096 bytes, respectively.
 */

#define _GNU_SOURCE /* Obtain O_DIRECT definition from <fcntl.h> */

#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <malloc.h>
#include <errno.h>

void exit_if_not_a_num(const char *str, const char *endptr);

int main(int argc, char *argv[])
{
    char *fname, *buf, *endptr;
    size_t length, alignment;
    off_t offset;
    int fd;
    ssize_t bytes_read;

    if (argc < 3 || strcmp(argv[1], "--help") == 0){
        printf("Usage: %s file length [offset [alignment]]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    fname = argv[1];
    length = (size_t) strtol(argv[2], &endptr, 10);
    exit_if_not_a_num(argv[2], endptr);

    offset = (argc > 3) ? (off_t) strtol(argv[3], &endptr, 10) : 0;
    exit_if_not_a_num(argv[3], endptr);

    alignment = (argc > 4) ? (size_t) strtol(argv[4], &endptr, 10) : 4096;
    exit_if_not_a_num(argv[4], endptr);

    /* Avoid landing at multiple memory boundaries e.g., 256-byte & 512-byte */
    buf = (char *) memalign(alignment * 2, alignment + length) + alignment;
    if (buf == NULL) {
        perror("memalign");
        exit(EXIT_FAILURE);
    }

    fd = open(fname, O_RDONLY | O_DIRECT);
    if (fd == -1) {
        if (errno == EINVAL)
            printf("%s: direct I/O is not supported by "
                "your file system\n", argv[0]);
        else
            perror("open");
        exit(EXIT_FAILURE);
    }

    if (lseek(fd, offset, SEEK_SET) == -1) {
        perror("lseek");
        exit(EXIT_FAILURE);
    }

    bytes_read = read(fd, buf, length);
    if (bytes_read == -1) {
        perror("read");
        exit(EXIT_FAILURE);
    }

    if (close(fd) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }

    printf("Read %ld bytes:\n", (long) bytes_read);
    printf("%s\n", buf);

    exit(EXIT_SUCCESS);
}

void exit_if_not_a_num(const char *str, const char *endptr)
{
    if (str == NULL) return;

    if (*str == '\0' || *endptr != '\0') {
        printf("%s is not a valid number\n", str);
        exit(EXIT_FAILURE);
    }
}
