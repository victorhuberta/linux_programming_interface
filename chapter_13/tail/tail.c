#define _XOPEN_SOURCE

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#define N_DEF_LINES 10
#define N_CHUNK_SZ 1024

int main(int argc, char **argv)
{
    long n_lines = N_DEF_LINES, fsize, n_chunk_sz, next_sz, left_sz;
    char opt, *endptr, *filename, *file_buf;
    int fd, i, j, rcount = 1, n_newlines = 0;
    off_t fcuri;

    if (argc < 2 || strcmp(argv[1], "--help") == 0) {
        printf("Usage: %s [-n num] file\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    while ((opt = getopt(argc, argv, "n:")) != -1) {
        switch (opt) {
            case 'n':
                n_lines = strtol(optarg, &endptr, 10);
                if (*optarg == '\0' || *endptr != '\0') {
                    printf("%s is not a valid integer\n", optarg);
                    exit(EXIT_FAILURE);
                }
                break;
            default:
                break;
        }
    }

    if (optind >= argc) {
        printf("Usage: %s [-n num] file\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    filename = argv[optind];
    if ((fd = open(filename, O_RDONLY)) == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }
    
    fsize = lseek(fd, 0, SEEK_END);
    if (fsize == -1) {
        perror("lseek");
        exit(EXIT_FAILURE);
    }

    n_chunk_sz = (fsize >= N_CHUNK_SZ) ? N_CHUNK_SZ : fsize;

    file_buf = malloc(fsize * sizeof *file_buf);
    if (file_buf == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    while (1) {
        next_sz = n_chunk_sz * rcount;
        left_sz = fsize - next_sz;
        if (left_sz < 0) {
            n_chunk_sz = N_CHUNK_SZ + left_sz;
            left_sz = 0;
        }
        if (n_chunk_sz <= 0) break;

        fcuri = lseek(fd, left_sz, SEEK_SET);
        if (fcuri == -1) {
            perror("lseek");
            exit(EXIT_FAILURE);
        }
 
        if (read(fd, file_buf + fcuri, n_chunk_sz) == -1) {
            perror("read");
            exit(EXIT_FAILURE);
        }

        if (fcuri == 0) {
            for (i = 0; i < fsize; ++i)
                printf("%c", file_buf[i]);
            break;
        }

        for (i = fcuri + n_chunk_sz - 1; i >= fcuri; --i) {
            if (file_buf[i] == '\n') ++n_newlines;
            if (n_newlines > n_lines) {
                for (j = i + 1; j < fsize; ++j)
                    printf("%c", file_buf[j]);
                goto finish_printing;
            }
        }

        ++rcount;
    }

finish_printing:

    if (close(fd) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }
    free(file_buf);

    exit(EXIT_SUCCESS);
}
