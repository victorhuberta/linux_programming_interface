#define _XOPEN_SOURCE 500

#include <stdlib.h>
#include <stdio.h>
#include <ftw.h>

static void usage_err(char *prog, char *msg);
static int print_entry(const char *fpath, const struct stat *sb,
    int typeflag, struct FTW *ftwbuf);

typedef struct FileRecord {
    int dir, reg, lnk, blk, chr, fifo, sock;
} FileRecord;

static FileRecord *update_filerec(mode_t st_mode);
static void print_filerec(FileRecord *filerec);
static int calc_total(FileRecord *filerec);

int main(int argc, char **argv)
{
    int opt, flags;
    char *dirpath = NULL;

    while ((opt = getopt(argc, argv, "dmp")) != -1) {
        switch (opt) {
            case 'd':
                flags |= FTW_DEPTH;
                break;
            case 'm':
                flags |= FTW_MOUNT;
                break;
            case 'p':
                flags |= FTW_PHYS;
                break;
            default:
                usage_err(argv[0], NULL);
                break;
        }
    }

    if (optind != argc - 1)
        usage_err(argv[0], NULL);

    dirpath = argv[optind];

    if (nftw(dirpath, print_entry, 10, flags) != 0) {
        perror("nftw");
        exit(EXIT_FAILURE);
    }

    print_filerec(update_filerec(0));

    exit(EXIT_SUCCESS);
}

static void usage_err(char *prog, char *msg)
{
    if (msg != NULL)
        fprintf(stderr, "%s\n", msg);

    fprintf(stderr, "Usage: %s [-dmp] dirpath\n", prog);
    fprintf(stderr, "\t-d Use FTW_DEPTH flag\n");
    fprintf(stderr, "\t-m Use FTW_MOUNT flag\n");
    fprintf(stderr, "\t-p Use FTW_PHYS flag\n");

    exit(EXIT_FAILURE);
}

/* Print every nftw entry with the following format:
 * [file-type] [type-flag] [i-node no.] [basename]
 *
 * Examples:
 * d D    2327983 dir
 * - F    2327984     a
 * ...
 */
static int print_entry(const char *fpath, const struct stat *sb,
    int typeflag, struct FTW *ftwbuf)
{
    update_filerec(sb->st_mode);
    switch (sb->st_mode & S_IFMT) {
        case S_IFDIR: printf("d"); break;
        case S_IFREG: printf("-"); break;
        case S_IFLNK: printf("l"); break;
        case S_IFBLK: printf("b"); break;
        case S_IFCHR: printf("c"); break;
        case S_IFIFO: printf("p"); break;
        case S_IFSOCK: printf("s"); break;
        default: printf("?"); break;
    }

    printf(" %-4.3s",
        (typeflag == FTW_D) ? "D" : (typeflag == FTW_F) ? "F" :
        (typeflag == FTW_DNR) ? "DNR" : (typeflag == FTW_NS) ? "NS" :
        (typeflag == FTW_DP) ? "DP" : (typeflag == FTW_SL) ? "SL" :
        (typeflag == FTW_SLN) ? "SLN" : " ");

    if (typeflag != FTW_NS) {
        printf("%-9ld ", (long) sb->st_ino);
    } else {
        printf("%10s", "");
    }
    
    printf("%*s", 4 * ftwbuf->level, ""); /* Indent according to level */
    printf("%s\n", &fpath[ftwbuf->base]); /* Print basename */
    return 0;
}

static FileRecord *update_filerec(mode_t st_mode)
{
    static FileRecord filerec = {0};

    switch (st_mode & S_IFMT) {
        case S_IFDIR: ++filerec.dir; break;
        case S_IFREG: ++filerec.reg; break;
        case S_IFLNK: ++filerec.lnk; break;
        case S_IFBLK: ++filerec.blk; break;
        case S_IFCHR: ++filerec.chr; break;
        case S_IFIFO: ++filerec.fifo; break;
        case S_IFSOCK: ++filerec.sock; break;
        default: return &filerec;
    }

    return &filerec;
}

static void print_filerec(FileRecord *filerec)
{
    int total = calc_total(filerec);

    printf("DIR = %d (%.2f%%)\n", filerec->dir,
        ((double) filerec->dir / total) * 100);
    printf("REG = %d (%.2f%%)\n", filerec->reg,
        ((double) filerec->reg / total) * 100);
    printf("LNK = %d (%.2f%%)\n", filerec->lnk,
        ((double) filerec->lnk / total) * 100);
    printf("BLK = %d (%.2f%%)\n", filerec->blk,
        ((double) filerec->blk / total) * 100);
    printf("CHR = %d (%.2f%%)\n", filerec->chr,
        ((double) filerec->chr / total) * 100);
    printf("FIFO = %d (%.2f%%)\n", filerec->fifo,
        ((double) filerec->fifo / total) * 100);
    printf("SOCK = %d (%.2f%%)\n", filerec->sock,
        ((double) filerec->sock / total) * 100);
    printf("TOTAL = %d\n", total);
}

static int calc_total(FileRecord *filerec)
{
    return filerec->dir + filerec->reg + filerec->lnk + filerec->blk +
        filerec->chr + filerec->fifo + filerec->sock;
}
