#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <linux/limits.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>
#include <stdio.h>

char *vhz_realpath(const char *path, char *resolved_path);
static char *canonicalize_path(const char *path, char *canonicalized);
static char *transform_path(char *path, char *found);
static char *use_cwd_on_relative(const char *path, char *buffer);
static char *path_to_parent(char *path);
static char *append_path(char *head, char *tail);
static char *dereference_path(char *path);
static char *get_link_content(char *path);
static char *get_dereferenced(char *path, char *link_content);

int main(int argc, char **argv)
{
    char *resolved_path = NULL;

    if (argc != 2 || strcmp(argv[1], "--help") == 0) {
        fprintf(stderr, "Usage: %s path\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    resolved_path = vhz_realpath(argv[1], NULL);
    if (resolved_path == NULL) {
        perror("vhz_realpath");
        exit(EXIT_FAILURE);
    }

    printf("%s\n", resolved_path);
    free(resolved_path);

    exit(EXIT_SUCCESS);
}

char *vhz_realpath(const char *path, char *resolved_path)
{
    int is_mallocd = 0; /* to free or not to free. */

    if (path == NULL) { errno = EINVAL; return NULL; }

    /* Put resolved_path on heap instead. */
    if (resolved_path == NULL) {
        resolved_path = malloc(PATH_MAX * sizeof *resolved_path);
        if (resolved_path == NULL) return NULL;
        is_mallocd = 1;
    }

    if (canonicalize_path(path, resolved_path) == NULL) {
        if (is_mallocd == 1) free(resolved_path);
        return NULL;
    }

    return resolved_path;
}

#define N_TMPBUF_LEN 80
static char *canonicalize_path(const char *path, char *canonicalized)
{
    char itembuf[N_TMPBUF_LEN] = {0};
    int i = 0, t = 0; /* index for itembuf */

    memset(canonicalized, '\0', PATH_MAX);

    if (use_cwd_on_relative(path, canonicalized) == NULL)
        return NULL;

    for (i = 0; ; ++i) {
        if (path[i] == '/' || path[i] == '\0') {
            if (transform_path(canonicalized, itembuf) == NULL)
                return NULL;

            memset(itembuf, '\0', strlen(itembuf)); /* always clear itembuf */
            t = 0; /* reset itembuf index */
            
            if (path[i] == '\0') break; else continue;
        }

        itembuf[t++] = path[i];
    }

    return canonicalized;
}

#define PARENT_DIR ".."
#define CURRENT_DIR "."
static char *transform_path(char *path, char *found)
{
    char *dereferenced = NULL;

    if (strcmp(found, PARENT_DIR) == 0) {
        if (path_to_parent(path) == NULL)
            return NULL;
    } else if (strcmp(found, CURRENT_DIR) == 0) {
        /* Do nothing. */
    } else if (strlen(found) == 0) {
        /* Do nothing. */
    } else {
        if (append_path(path, found) == NULL)
            return NULL;
        if ((dereferenced = dereference_path(path)) == NULL)
            return NULL;
        if (dereferenced == path) /* path is not a symlink */
            return path;

        if (canonicalize_path(dereferenced, path) == NULL) {
            free(dereferenced);
            return NULL;
        }

        free(dereferenced);
    }

    return path;
}

static char *use_cwd_on_relative(const char *path, char *buffer)
{
    if (path[0] != '/') return getcwd(buffer, PATH_MAX);
    else return buffer;
}

static char *path_to_parent(char *path)
{
    char *localbuf = strndup(path, PATH_MAX);
    if (localbuf == NULL) return NULL;

    strncpy(path, dirname(localbuf), PATH_MAX);
    free(localbuf);

    return path;
}

static char *append_path(char *head, char *tail)
{
    int len = strlen(head);

    if ((len == 0) || (head[len - 1] != '/'))
        head[len] = '/';

    return strncat(head, tail, PATH_MAX - len);
}

static char *dereference_path(char *path)
{
    struct stat sb = {0};
    char *dereferenced = NULL, *link_content = NULL;

    if (lstat(path, &sb) == -1)
        return NULL;

    if ((sb.st_mode & S_IFMT) == S_IFLNK) {
        if ((link_content = get_link_content(path)) == NULL)
            return NULL;

        if ((dereferenced = get_dereferenced(path, link_content)) == NULL) {
            free(link_content);
            return NULL;
        }

        free(link_content);
        return dereferenced;
    }

    return path;
}

static char *get_link_content(char *path)
{
    int buf_sz = 0;
    char *link_content = malloc(PATH_MAX * sizeof *link_content);
    if (link_content == NULL) return NULL;

    buf_sz = readlink(path, link_content, PATH_MAX);
    if (buf_sz == -1) {
        free(link_content);
        return NULL;
    }
    link_content[buf_sz] = '\0'; /* terminate string */

    return link_content;
}

static char *get_dereferenced(char *path, char *link_content)
{
    char *localbuf = NULL;
    char *dereferenced = malloc(PATH_MAX * sizeof *dereferenced);
    if (dereferenced == NULL) return NULL;

    if (link_content[0] != '/') {
        localbuf = strndup(path, PATH_MAX);
        strncpy(dereferenced, dirname(localbuf), PATH_MAX);
        free(localbuf);

        /* Set new terminating null byte. */
        dereferenced[strlen(dereferenced)] = '/';
        strncat(dereferenced, link_content, PATH_MAX - strlen(dereferenced));
    } else {
        strncpy(dereferenced, link_content, PATH_MAX);
    }

    return dereferenced;
}
