#!/bin/bash
# Test ex18-3

EXE="$1"
input_list="tests_input.list"
default_cmd="./$EXE $input 2>&1"
simulated_cmd="realpath $input 2>&1"

exec_bit=0
test_no=0
should_fail=0
ok=0
failed=0
while read -r test_input
do
    case "$test_input" in
    \[tests\])
        exec_bit=0
        echo "Running tests..."
        continue
        ;;
    \[preparation\])
        exec_bit=1
        echo "Preparing for next tests..."
        continue
        ;;
    \[cleanup\])
        exec_bit=1
        echo "Performing clean up..."
        continue
        ;;
    \[fail\])
        should_fail=1
        continue
        ;;
    esac

    [ -z "$test_input" ] && continue

    test_input=$(eval "echo $test_input")

    if [ $exec_bit -eq 1 ]; then
        $test_input &> /dev/null
        continue
    fi

    output=$(./$EXE "$test_input" 2>&1)
    result=$?
    echo
    echo -n "Test #${test_no}: "
    if (( (result == 0 && should_fail == 0) || \
        (result != 0 && should_fail == 1) )); then
        echo "OK"
        ok=$[ $ok + 1]
    else
        echo "FAILED"
        failed=$[ $failed + 1 ]
    fi
    echo "$output"
    echo "------"
    test_no=$[ $test_no + 1 ]
    should_fail=0
done < "$input_list"

echo "Tests finished."
echo
echo "Result: $ok OK ; $failed FAILED"
