#define _XOPEN_SOURCE 500

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>

#include <fcntl.h>
#include <sys/stat.h>

#include <string.h>
#include <errno.h>

#include <linux/limits.h> /* PATH_MAX */

char *vhz_getcwd(char *buf, size_t size);
static char *traverse_dir(struct stat *prev, char *buf, size_t size);
static int cmpfiles(struct stat *first, struct stat *second);
static char *find_dir(struct stat *target, char *buf, size_t size);
static char *prepend_path(char *buf, size_t size, char *d_name);

int main(int argc, char **argv)
{
    char *cwd = malloc(PATH_MAX * sizeof *cwd);
    if (cwd == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    if (vhz_getcwd(cwd, PATH_MAX) == NULL) {
        perror("vhz_getcwd");
        exit(EXIT_FAILURE);
    }

    printf("%s\n", cwd);
    free(cwd);

    exit(EXIT_SUCCESS);
}

/* Find the absolute path of current working directory.
 *
 * # Algorithm 
 * 1: Save the current working directory file descriptor by
 * calling open(".").
 *
 * 2A: Find out the i-node number and device ID of current working
 * directory.
 *
 * 2B: Compare the found i-node number and device ID to the
 * previously found i-node number and device ID. If they match,
 * you have reached the root directory. Stop iteration.
 *
 * 3: Change the current working directory to its parent by
 * calling chdir("..").
 *
 * 4: Use opendir() and readdir() to find the directory which
 * has the same i-node number and device ID as the previous
 * current working directory.
 *
 * 5: Prepend the directory name to the path. It should start with
 * a '/'.
 *
 * 6: Go back to (2A).
 */
char *vhz_getcwd(char *buf, size_t size)
{
    int orig = -1;

    if (buf == NULL) { errno = EINVAL; return NULL; }
    if (size == 0) { errno = ERANGE; return NULL; }

    memset(buf, '\0', size);

    if ((orig = open(".", O_RDONLY)) == -1) return NULL;

    traverse_dir(NULL, buf, size);

    if (fchdir(orig) == -1) return NULL;

    return buf;
}

static char *traverse_dir(struct stat *prev, char *buf, size_t size)
{
    struct stat curr = {0};

    if (stat(".", &curr) == -1) return NULL;

    if (prev != NULL) {
        if (cmpfiles(prev, &curr))
            return buf;
    }

    if (chdir("..") == -1) return NULL;
    if (find_dir(&curr, buf, size) == NULL) return NULL;    

    return traverse_dir(&curr, buf, size);
}

static int cmpfiles(struct stat *first, struct stat *second)
{
    return ((first->st_dev == second->st_dev) &&
        (first->st_ino == second->st_ino));
}

static char *find_dir(struct stat *target, char *buf, size_t size)
{
    DIR *dirp = NULL;
    struct dirent *entry = NULL;
    struct stat curr = {0};

    dirp = opendir(".");
    if (dirp == NULL) return NULL;

    errno = 0;
    while ((entry = readdir(dirp)) != NULL) {
        if (strcmp(entry->d_name, "..") == 0)
            continue;

        if (stat(entry->d_name, &curr) == -1)
            break;

        if (cmpfiles(&curr, target)) {
            if (prepend_path(buf, size, entry->d_name) == NULL) {
                if (closedir(dirp) == -1) return NULL;
                return NULL;
            }
        }

        errno = 0;
    }
    if (errno != 0) return NULL;
    if (closedir(dirp) == -1) return NULL;

    return buf;
}

static char *prepend_path(char *buf, size_t size, char *d_name)
{
    char *tmpbuf = malloc(size * sizeof *tmpbuf);
    if (tmpbuf == NULL) return NULL;

    memset(tmpbuf, '\0', size); /* always clean tmpbuf */

    if (strcmp(d_name, ".") != 0)
        strncpy(tmpbuf, d_name, size);

    if (strlen(buf) != 0)
        tmpbuf[strlen(tmpbuf)] = '/'; /* safe because of memset */

    strncat(tmpbuf, buf, size - strlen(tmpbuf));
    strncpy(buf, tmpbuf, size);

    free(tmpbuf);

    return buf;
}
