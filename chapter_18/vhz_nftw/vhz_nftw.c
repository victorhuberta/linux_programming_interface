#define _XOPEN_SOURCE 500

#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <linux/limits.h>
#include <unistd.h>

/* Type flags */
#define FTW_F 1
#define FTW_D 2
#define FTW_DNR 3
#define FTW_NS 4
#define FTW_DP 5
#define FTW_SL 6
#define FTW_SLN 7

/* nftw() flags */
#ifdef _GNU_SOURCE
#define FTW_ACTIONRETVAL 0x1
    #define FTW_CONTINUE 2
    #define FTW_SKIP_SIBLINGS 3
    #define FTW_SKIP_SUBTREE 4
    #define FTW_STOP 5
#endif
#define FTW_CHDIR 0x2
#define FTW_DEPTH 0x4
#define FTW_MOUNT 0x8
#define FTW_PHYS 0xF

struct FTW {
    int base, level;
};

static void usage_err(char *prog, char *msg);
static int print_entry(const char *fpath, const struct stat *sb,
    int typeflag, struct FTW *ftwbuf);

int nftw(const char *dirpath, int (*fn) (const char *fpath,
    const struct stat *sb, int typeflag, struct FTW *ftwbuf),
        int nopenfd, int flags);
static int wsubdir(const char *dirpath, int (*fn) (const char *fpath,
    const struct stat *sb, int typeflag, struct FTW *ftwbuf), int nopenfd,
        int flags, int level);
static int apply_fn_on_entry(const char *dirpath, int (*fn) (const char *fpath,
    const struct stat *sb, int typeflag, struct FTW *ftwbuf), int nopenfd,
        int flags, int level, char *d_name);

static char *get_fpath(const char *dirpath, const char *d_name);
static char *get_fpath_r(const char *dirpath, const char *d_name, char *fpath);

static struct stat *get_stat(const char *fpath, int follow);
static struct stat *get_stat_r(const char *fpath,
    int follow, struct stat *sb);

static int get_ftype(const struct stat *sb, const char *fpath, int flags);

static struct FTW *get_ftwbuf(const char *fpath,
    const char *d_name, const int level);
static struct FTW *get_ftwbuf_r(const char *fpath,
    const char *d_name, const int level, struct FTW *ftwbuf);

#ifndef UNIT_TESTS
int main(int argc, char **argv)
{
    int opt, flags;
    char *dirpath = NULL;

    while ((opt = getopt(argc, argv, "dmp")) != -1) {
        switch (opt) {
            case 'd':
                flags |= FTW_DEPTH;
                break;
            case 'm':
                flags |= FTW_MOUNT;
                break;
            case 'p':
                flags |= FTW_PHYS;
                break;
            default:
                usage_err(argv[0], NULL);
                break;
        }
    }

    if (optind != argc - 1)
        usage_err(argv[0], NULL);

    dirpath = argv[optind];

    if (nftw(dirpath, print_entry, 10, flags) != 0) {
        perror("nftw");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}
#endif /* UNIT_TESTS */

static void usage_err(char *prog, char *msg)
{
    if (msg != NULL)
        fprintf(stderr, "%s\n", msg);

    fprintf(stderr, "Usage: %s [-dmp] dirpath\n", prog);
    fprintf(stderr, "\t-d Use FTW_DEPTH flag\n");
    fprintf(stderr, "\t-m Use FTW_MOUNT flag\n");
    fprintf(stderr, "\t-p Use FTW_PHYS flag\n");

    exit(EXIT_FAILURE);
}

/* Print every nftw entry with the following format:
 * [file-type] [type-flag] [i-node no.] [basename]
 *
 * Examples:
 * d D    2327983 dir
 * - F    2327984     a
 * ...
 */
static int print_entry(const char *fpath, const struct stat *sb,
    int typeflag, struct FTW *ftwbuf)
{
    if (typeflag == FTW_NS) {
        printf("x");
    } else {
        switch (sb->st_mode & S_IFMT) {
            case S_IFDIR: printf("d"); break;
            case S_IFREG: printf("-"); break;
            case S_IFLNK: printf("l"); break;
            case S_IFBLK: printf("b"); break;
            case S_IFCHR: printf("c"); break;
            case S_IFIFO: printf("p"); break;
            case S_IFSOCK: printf("s"); break;
            default: printf("?"); break;
        }
    }

    printf(" %-4.3s",
        (typeflag == FTW_D) ? "D" : (typeflag == FTW_F) ? "F" :
        (typeflag == FTW_DNR) ? "DNR" : (typeflag == FTW_NS) ? "NS" :
        (typeflag == FTW_DP) ? "DP" : (typeflag == FTW_SL) ? "SL" :
        (typeflag == FTW_SLN) ? "SLN" : " ");

    if (typeflag != FTW_NS) {
        printf("%-9ld ", (long) sb->st_ino);
    } else {
        printf("%10s", "");
    }
    
    printf("%*s", 4 * ftwbuf->level, ""); /* Indent according to level */
    printf("%s\n", &fpath[ftwbuf->base]); /* Print basename */
    return 0;
}

/* High level overview:
 * Traverse the specified directory subtrees and apply an operation
 * on each of them.
 *
 * # Algorithm
 * 1: opendir() dirpath.
 * 2: use readdir() to iterate every sub file and directory.
 * 3: Take the relative file path OR absolute file path depending
 * on whether or not dirpath is absolute.
 * 4: Get the stat structure of the file.
 * 5: Get the type of the file...
 * - FTW_F -- regular file
 * - FTW_D -- directory
 * - FTW_DNR -- a directory which can't be read.
 * - FTW_NS -- stat() failed on file, but it isn't a symlink.
 * - FTW_DP -- a directory and FTW_DEPTH was specified in flags.
 * - FTW_SL -- a symlink (FTW_PHYS was set).
 * - FTW_SLN -- a dangling symlink.
 *
 * 6: Get the base name index (using strstr()).
 * 7: Get the tree level (root is 0).
 * 8: Call fn with information gathered from 3 to 7.
 * 9: ...
 */
int nftw(const char *dirpath, int (*fn) (const char *fpath,
    const struct stat *sb, int typeflag, struct FTW *ftwbuf),
        int nopenfd, int flags)
{
    return apply_fn_on_entry(dirpath, fn, nopenfd, flags, 0, ".");
}

static int wsubdir(const char *dirpath, int (*fn) (const char *fpath,
    const struct stat *sb, int typeflag, struct FTW *ftwbuf), int nopenfd,
        int flags, int level)
{
    DIR *dirp = NULL;
    struct dirent *entry = NULL;
    int result = 0;

    dirp = opendir(dirpath);
    if (dirp == NULL) return -1;

    errno = 0;
    while ((entry = readdir(dirp)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 ||
            strcmp(entry->d_name, "..") == 0)
            continue;
        
        result = apply_fn_on_entry(dirpath, fn, nopenfd,
            flags, level, entry->d_name);

        if (result != 0) return result;

        errno = 0;
    }

    if (errno != 0) return -1;
    if (closedir(dirp) == -1) return -1;

    return result;
}

static int apply_fn_on_entry(const char *dirpath, int (*fn) (const char *fpath,
    const struct stat *sb, int typeflag, struct FTW *ftwbuf), int nopenfd,
        int flags, int level, char *d_name)
{
    struct stat sb = {0};
    int ftype = -1;
    struct FTW ftwbuf = {0};
    int result = 0;
    char fpath[PATH_MAX] = {0};

    get_fpath_r(dirpath, d_name, fpath);
    get_stat_r(fpath, ((flags & FTW_PHYS) == 0) ? 1 : 0, &sb);

    ftype = get_ftype(&sb, fpath, flags);
    get_ftwbuf_r(fpath, d_name, level, &ftwbuf);

    if (ftype == FTW_DP) {
        result = wsubdir(fpath, fn, nopenfd, flags, level + 1);
        if (result != 0) return result;

        result = (*fn)(fpath, &sb, ftype, &ftwbuf);
    } else if (ftype == FTW_D) {
        result = (*fn)(fpath, &sb, ftype, &ftwbuf);
        if (result != 0) return result;

        result = wsubdir(fpath, fn, nopenfd, flags, level + 1);
    } else {
        result = (*fn)(fpath, &sb, ftype, &ftwbuf);
    }

    return result;
}

static char *get_fpath(const char *dirpath, const char *d_name)
{
    static char fpath[PATH_MAX] = {0};

    memset(fpath, '\0', PATH_MAX);

    strncpy(fpath, dirpath, PATH_MAX);
    if (strcmp(d_name, ".") == 0) return fpath;

    fpath[strlen(fpath)] = '/';
    strncat(fpath, d_name, PATH_MAX - strlen(fpath));

    return fpath;
}

static char *get_fpath_r(const char *dirpath, const char *d_name, char *fpath)
{
    if (fpath == NULL) {
        errno = EINVAL;
        return NULL;
    }

    memset(fpath, '\0', PATH_MAX);
    strncpy(fpath, get_fpath(dirpath, d_name), PATH_MAX);

    return fpath;
}

static struct stat *get_stat(const char *fpath, int follow)
{
    static struct stat sb = {0};

    if (follow == 1) {
        if (stat(fpath, &sb) == -1) {
            /* Change file type to S_IFREG if it is a dangling symlink. */
            if (errno == ENOENT) {
                if (lstat(fpath, &sb) == -1)
                    return NULL;
                sb.st_mode &= ~S_IFLNK;
                sb.st_mode |= S_IFREG;
                return &sb;
            }

            return NULL;
        }
    } else {
        if (lstat(fpath, &sb) == -1)
            return NULL;
    }

    return &sb;
}

static struct stat *get_stat_r(const char *fpath, int follow, struct stat *sb)
{
    struct stat *tmp;

    if (! sb) { errno = EINVAL; return NULL; }
    memset(sb, '\0', sizeof *sb);
    
    tmp = get_stat(fpath, follow);
    if (! tmp) { errno = EINVAL; return NULL; }

    memcpy(sb, tmp, sizeof *sb);

    return sb;
}

static int get_ftype(const struct stat *sb, const char *fpath, int flags)
{
    if (sb == NULL || sb->st_mode == 0) return FTW_NS;

    switch (sb->st_mode & S_IFMT) {
        case S_IFREG:
            if (access(fpath, R_OK) == 0) {
                return FTW_F;
            } else {
                return FTW_SLN;
            }
        case S_IFDIR:
            if (access(fpath, R_OK) == 0) {
                if ((flags & FTW_DEPTH) == 0) {
                    return FTW_D;
                } else {
                    return FTW_DP;
                }
            } else {
                return FTW_DNR;
            }
        case S_IFLNK: return FTW_SL;
        default: return -1;
    }
}

static struct FTW *get_ftwbuf(const char *fpath,
    const char *d_name, const int level)
{
    static struct FTW ftwbuf = {0};
    char *baseptr = strstr(fpath, d_name);

    if (baseptr == NULL) {
        ftwbuf.base = 0;
    } else {
        ftwbuf.base = baseptr - fpath;
    }

    ftwbuf.level = level;

    return &ftwbuf;
}

static struct FTW *get_ftwbuf_r(const char *fpath,
    const char *d_name, const int level, struct FTW *ftwbuf)
{
    if (ftwbuf == NULL) {
        errno = EINVAL;
        return NULL;
    }

    memcpy(ftwbuf, get_ftwbuf(fpath, d_name, level), sizeof *ftwbuf);

    return ftwbuf;
}
