#include "minunit.h"
#include "vhz_nftw.c"

int tests_run = 0;

static char *test_get_fpath(void)
{
    const char dirpath[] = "/etc";
    const char d_name[] = "passwd";
    int result = 0;
    const char *fpath = get_fpath(dirpath, d_name);
    mu_assert("get_fpath: fpath == NULL", fpath != NULL);

    result = strcmp(fpath, "/etc/passwd");

    mu_assert("get_fpath: fpath != /etc/passwd", result == 0);
    return 0;
}

static char *test_get_stat(void)
{
    const char d_name[] = "/etc/passwd";
    int follow = 1;
    struct stat *sb = get_stat(d_name, follow);
    
    mu_assert("get_stat: sb == NULL", sb != NULL);
    return 0;
}

static char *test_get_ftype(void)
{
    const char d_name[] = "/etc/passwd";
    int flags = 0;
    struct stat *sb = get_stat(d_name, 1);
    int ftype = get_ftype(sb, d_name, flags);

    mu_assert("get_ftype: ftype != FTW_F", ftype == FTW_F);
    return 0;
}

static char *test_get_ftwbuf(void)
{
    struct FTW *ftwbuf;
    const char *fpath = "/etc/passwd";
    const char *d_name = "passwd";
    const int level = 0;

    ftwbuf = get_ftwbuf(fpath, d_name, level);

    mu_assert("get_ftwbuf: ftwbuf->base != 5", ftwbuf->base == 5);
    mu_assert("get_ftwbuf: ftwbuf->level != 0", ftwbuf->level == 0);
    return 0;
}

static char *test_wsubdir(void)
{
    /* TODO: Implement the test. */
}

static char *all_tests(void)
{
    mu_run_test(test_get_fpath);
    mu_run_test(test_get_stat);
    mu_run_test(test_get_ftype);
    mu_run_test(test_get_ftwbuf);
    return 0;
}

#ifdef UNIT_TESTS
int main(int argc, char **argv)
{
    char *result = all_tests();

    if (result != 0) {
        printf("FAILED: %s\n", result);
    } else {
        printf("ALL TESTS PASSED\n");
    }

    printf("Tests run: %d\n", tests_run);
    return result != 0;
}
#endif /* UNIT_TESTS */
