#define _XOPEN_SOURCE

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/acl.h>
#include <acl/libacl.h> /* Linux-specific extensions: acl_get_perm() */
#include <unistd.h>
#include <pwd.h>
#include <grp.h>

static void usage_err(const char *prog, const char *msg);
static uid_t vhz_getuid(const char *unm);
static char *getunm(uid_t uid);
static gid_t vhz_getgid(const char *gnm);
static char *getgnm(gid_t gid);
static acl_perm_t get_aclmask(acl_t acl);
static int write_permset(acl_permset_t permset, acl_perm_t aclmask);

int main(int argc, char **argv)
{
    int opt, entry_id, status, is_matched = 0;
    uid_t uid = -1;
    gid_t gid = -1;
    char *unm = NULL, *gnm = NULL;
    char *endptr, *pathname = NULL;
    struct stat st;
    acl_t acl;
    acl_entry_t entry;
    acl_tag_t tag_type;
    acl_permset_t permset;
    acl_perm_t aclmask = -1;
    void *qualifier;

    while ((opt = getopt(argc, argv, "u:g:")) != -1) {
        switch (opt) {
            case 'u':
                uid = strtol(optarg, &endptr, 10);
                if (*optarg == '\0' || *endptr != '\0')
                    unm = optarg;
                break;
            case 'g':
                gid = strtol(optarg, &endptr, 10);
                if (*optarg == '\0' || *endptr != '\0')
                    gnm = optarg;
                break;
            default:
                usage_err(argv[0], NULL);
                break;
        }
    }

    if (uid != -1 || unm != NULL) {
        if (unm == NULL) {
            /* Actually, username is not required to check qualifier. */
            unm = getunm(uid);
            if (unm == NULL) usage_err(argv[0], "Invalid UID.");
        } else {
            uid = vhz_getuid(unm);
            if (uid == -1) usage_err(argv[0], "Invalid username.");
        }
    }

    if (gid != -1 || gnm != NULL) {
        if (gnm == NULL) {
            /* Actually, group's name is not required to check qualifier. */
            gnm = getgnm(gid);
            if (gnm == NULL) usage_err(argv[0], "Invalid GID.");
        } else {
            gid = vhz_getgid(gnm);
            if (gid == -1) usage_err(argv[0], "Invalid group's name.");
        }
    }

    if (uid == -1 && gid == -1)
        usage_err(argv[0], NULL);

    if (optind + 1 != argc)
        usage_err(argv[0], "Invalid number of arguments.");

    pathname = argv[optind];

    if (stat(pathname, &st) == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    acl = acl_get_file(pathname, ACL_TYPE_ACCESS);
    if (acl == NULL) {
        perror("acl_get_file");
        exit(EXIT_FAILURE);
    }

    aclmask = get_aclmask(acl);

    for (entry_id = ACL_FIRST_ENTRY; ; entry_id = ACL_NEXT_ENTRY) {
        if ((status = acl_get_entry(acl, entry_id, &entry)) != 1) {
            if (status == -1) {
                perror("acl_get_entry");
                exit(EXIT_FAILURE);
            }
            break;
        }

        qualifier = acl_get_qualifier(entry);

        acl_get_tag_type(entry, &tag_type);
        switch (tag_type) {
            case ACL_USER_OBJ:
                if (st.st_uid == uid) {
                    is_matched = 1;
                    printf("%-10s", "user_obj");
                    printf("%23s", "");
                }
                break;
            case ACL_USER:
                if (*((uid_t *) qualifier) == uid) {
                    is_matched = 1;
                    printf("%-10s", "user");
                    printf("%4.4ld - %-16.15s", (long) uid, unm);
                }
                break;
            case ACL_GROUP_OBJ:
                if (st.st_gid == gid) {
                    is_matched = 1;
                    printf("%-10s", "group_obj");
                    printf("%23s", "");
                }
                break;
            case ACL_GROUP:
                if (*((gid_t *) qualifier) == gid) {
                    is_matched = 1;
                    printf("%-10s", "group");
                    printf("%4.4ld - %-16.15s", (long) gid, gnm);
                }
                break;
            default:
                break;
        }

        if (qualifier != NULL && acl_free(qualifier) == -1) {
            perror("acl_free");
            exit(EXIT_FAILURE);
        }

        if (is_matched == 1) {
            if (acl_get_permset(entry, &permset) == -1) {
                perror("acl_get_permset");
                exit(EXIT_FAILURE);
            }

            switch (tag_type) {
                case ACL_USER:
                case ACL_GROUP_OBJ:
                case ACL_GROUP:
                    if (aclmask != -1)
                        status = write_permset(permset, aclmask);
                    else
                        status = write_permset(permset, -1);
                    break;
                default:
                    status = write_permset(permset, -1);
                    break;
            }

            if (status == -1) {
                perror("write_permset");
                exit(EXIT_FAILURE);
            }

            printf("\n");
            is_matched = 0;
        }
    }

    if (acl_free(acl) == -1) {
        perror("acl_free");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}

static void usage_err(const char *prog, const char *msg)
{
    if (msg != NULL)
        fprintf(stderr, "%s\n", msg);

    fprintf(stderr, "Usage: %s [options] file\n", prog);
    fprintf(stderr, "    -u username / user ID\n");
    fprintf(stderr, "    -g group's name / group ID\n");

    exit(EXIT_FAILURE);
}

static uid_t vhz_getuid(const char *unm)
{
    struct passwd *pw;

    errno = 0;
    pw = getpwnam(unm); 

    return (pw == NULL) ? -1 : pw->pw_uid;
}

static char *getunm(uid_t uid)
{
    struct passwd *pw;

    errno = 0;
    pw = getpwuid(uid);
    
    return (pw == NULL) ? NULL : pw->pw_name;
}

static gid_t vhz_getgid(const char *gnm)
{
    struct group *gr;

    errno = 0;
    gr = getgrnam(gnm); 

    return (gr == NULL) ? -1 : gr->gr_gid;
}

static char *getgnm(gid_t gid)
{
    struct group *gr;

    errno = 0;
    gr = getgrgid(gid); 

    return (gr == NULL) ? NULL : gr->gr_name;
}

static acl_perm_t get_aclmask(acl_t acl)
{
    int entry_id, status;
    acl_entry_t entry;
    acl_tag_t tag_type;
    acl_permset_t permset;
    acl_perm_t aclmask = -1;

    for (entry_id = ACL_FIRST_ENTRY; ; entry_id = ACL_NEXT_ENTRY) {
        if ((status = acl_get_entry(acl, entry_id, &entry)) != 1) {
            if (status == -1) return -1;
            break;
        }

        if (acl_get_tag_type(entry, &tag_type) == -1) return -1;

        switch (tag_type) {
            case ACL_MASK:
                if (acl_get_permset(entry, &permset) == -1) {
                    perror("acl_get_permset");
                    exit(EXIT_FAILURE);
                }

                aclmask = 0;

                if ((status = acl_get_perm(permset, ACL_READ)) == 1)
                    aclmask |= ACL_READ;
                if ((status = acl_get_perm(permset, ACL_WRITE)) == 1)
                    aclmask |= ACL_WRITE;
                if ((status = acl_get_perm(permset, ACL_EXECUTE)) == 1)
                    aclmask |= ACL_EXECUTE;

                if (status == -1) {
                    perror("acl_get_perm");
                    exit(EXIT_FAILURE);
                }

                break;
            default:
                break;
        }
    }

    return aclmask;
}

static int write_permset(acl_permset_t permset, acl_perm_t aclmask)
{
    acl_perm_t eperm = 0;
    int status;

    if ((status = acl_get_perm(permset, ACL_READ)) == 1)
        eperm |= ACL_READ;
    if ((status = acl_get_perm(permset, ACL_WRITE)) == 1)
        eperm |= ACL_WRITE;
    if ((status = acl_get_perm(permset, ACL_EXECUTE)) == 1)
        eperm |= ACL_EXECUTE;

    if (status == -1) return -1;
    if (aclmask != -1) eperm &= aclmask;

    printf("%c", ((eperm & ACL_READ) != 0) ? 'r' : '-');
    printf("%c", ((eperm & ACL_WRITE) != 0) ? 'w' : '-');
    printf("%c", ((eperm & ACL_EXECUTE) != 0) ? 'x' : '-');

    return 0;
}
