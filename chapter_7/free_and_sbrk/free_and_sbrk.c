/*
 * Module: free_and_sbrk; @Author: Victor Huberta
 * ```
 * Demonstrate what happens to the program break
 * when memory is freed.
 * ```
 */

#define _BSD_SOURCE

#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#define MAX_ALLOCS 1000
#define MAX_BLOCK_SIZE 10000

long convert_tol(const char *var_name, const char *str);
ssize_t catch_err(const char *fname, ssize_t res);

int main(int argc, char *argv[])
{
    if (argc < 3 || strcmp(argv[1], "--help") == 0) {
        printf("Usage: %s num-allocs block-size [step [min [max]]]\n",
            argv[0]);
        exit(EXIT_FAILURE);
    }

    long num_allocs = catch_err("convert_tol",
        convert_tol("num-allocs", argv[1]));

    if (num_allocs > MAX_ALLOCS) {
        puts("num-allocs exceeds its maximum number.");
        printf("setting num-allocs to %d...\n", MAX_ALLOCS);
        num_allocs = MAX_ALLOCS;
    }

    long block_size = catch_err("convert_tol",
        convert_tol("block-size", argv[2]));

    if (block_size > MAX_BLOCK_SIZE) {
        puts("block-size exceeds its maximum number.");
        printf("setting block-size to %d...\n", MAX_BLOCK_SIZE);
        block_size = MAX_BLOCK_SIZE;
    }

    long free_step = (argc > 3) ? convert_tol("step", argv[3]) : 1;
    catch_err("free_step", free_step);

    long free_min = (argc > 4) ? convert_tol("min", argv[4]) : 1;
    catch_err("free_min", free_min);

    free_min = (free_min > 0) ? free_min : 1;

    long free_max = (argc > 5) ? convert_tol("max", argv[5]) : num_allocs;
    catch_err("free_max", free_max);

    free_max = (free_max <= num_allocs) ? free_max : num_allocs;

    printf("Initial program break: %10p\n", sbrk(0));
    printf("Allocating %d*%d bytes...\n", num_allocs, block_size);

    void *blocks[num_allocs];
    for (int i = 0; i < num_allocs; ++i) {
        blocks[i] = malloc(block_size);

        if (blocks[i] == NULL) {
            perror("malloc");
            exit(EXIT_FAILURE);
        }

        printf("Program break after malloc: %10p\n", sbrk(0));
    }

    printf("Program break is now: %10p\n", sbrk(0));
    printf("Freeing blocks from %d to %d in steps of %d...\n",
        free_min, free_max, free_step);

    for (int i = free_min - 1; i < free_max; i += free_step)
        free(blocks[i]);

    printf("After free(), program break is: %10p\n", sbrk(0));

    exit(EXIT_SUCCESS);
}

long convert_tol(const char *var_name, const char *str)
{
    if (str[0] == '\0') {
        errno = EINVAL;
        printf("%s can't be empty.\n", var_name);
        return -1;
    }

    errno = 0; /* important: reset errno */

    char *endptr;
    long res = strtol(str, &endptr, 10);

    if (errno != 0) {
        perror("strtol");
        return -1;
    }

    if (endptr[0] != '\0' || res <= 0) {
        errno = EINVAL;
        printf("%s is not a valid number.\n", var_name);
        return -1;
    }

    return res;
}

ssize_t catch_err(const char *fname, ssize_t res)
{
    if (res == -1) {
        perror(fname);
        exit(EXIT_FAILURE);
    } else {
        return res;
    }
}
