struct block {
    size_t size;
    struct block *prev;
    struct block *next;
};

struct block fblock;

void free(void *ptr)
{
    struct block *new;
    struct block last;

    find_last(&last);
    new = (struct block *) ptr;
    last.next = new;
}

void find_last(struct block *last) {
    struct block fbp;

    for (fbp = fblock; fbp.next != NULL; fbp = fbp.next);

    *last = fbp;
}
