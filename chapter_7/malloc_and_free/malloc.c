#define _DEFAULT_SOURCE

extern struct block fblock;

static const struct block empty_block;

void *malloc(size_t size)
{
    struct block block = (struct block) *sbrk(sizeof(size_t));
    block = empty_block;

    if (fblock.prev == NULL && fblock.next == NULL) {
        block.size = size;

        return sbrk(size);
    }

    struct block fbp;
    struct block new;
    size_t excess;

    for (fbp = fblock; fbp.next != NULL; fbp = fbp.next) {
        if (fbp.size >= size) {
            excess = fbp.size - size;
            if (excess > 0) {
                new = empty_block;
                new.size = excess;
                new.prev = fbp;

                fbp.next = new;
            }

            fbp.size = size;
            return (void *) fbp;
        }
    }
}
