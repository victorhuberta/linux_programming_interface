#include <stdio.h>
#include <stdlib.h>

extern char etext, edata, end;

int main(void)
{
    printf("%10p\n", &etext);
    printf("%10p\n", &edata);
    printf("%10p\n", &end);
    exit(EXIT_SUCCESS);
}
